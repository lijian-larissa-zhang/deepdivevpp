PKG = LIBNL
LIBNL_VERSION = 3.2.25
LIBNL_SITE = https://www.infradead.org/~tgr/libnl/files
PKG_NAME = $(notdir $(CURDIR))
TARFILE_NAME = $(PKG_NAME)-$(LIBNL_VERSION)

# 1. Download source code
define LIBNL_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && wget $(LIBNL_SITE)/$(TARFILE_NAME).tar.gz
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && tar -xf $(TARFILE_NAME).tar.gz
endef

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define LIBNL_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define LIBNL_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && \
		PATH=$(PATH):$(ENV_PATH) \
		CC=aarch64-none-linux-gnu-gcc \
		./configure --host=aarch64-linux-gnu --prefix=$(INSTALL_DIR)
endef

# 4. Install compiled binaries
define LIBNL_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && \
		sudo PATH=$(PATH):$(ENV_PATH) \
		CC=aarch64-none-linux-gnu-gcc \
		make install
endef

# 5. Do closing work before finish
define LIBNL_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
