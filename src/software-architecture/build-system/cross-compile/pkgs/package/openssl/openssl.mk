PKG = OPENSSL
OPENSSL_VERSION = OpenSSL_1_1_1f
OPENSSL_SITE = https://github.com/openssl/openssl.git

PKG_NAME = $(notdir $(CURDIR))

# 1. Download source code
LOCAL_SRC = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_NAME)*)
ifeq ($(LOCAL_SRC),)
define OPENSSL_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone -b $($(PKG)_VERSION) $($(PKG)_SITE) $(BUILD_DIR)/$(PKG_NAME)
endef
else
define OPENSSL_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone $(LOCAL_SRC) $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define OPENSSL_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define OPENSSL_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && PATH=$(PATH):$(ENV_PATH) ./Configure linux-aarch64 --cross-compile-prefix=$(INSTALL_DIR)/bin/aarch64-none-linux-gnu- --prefix=$(INSTALL_DIR) --openssldir=$(INSTALL_DIR)
endef

# 4. Install compiled binaries
define OPENSSL_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && sudo PATH=$(PATH):$(ENV_PATH) make install
endef

# 5. Do closing work before finish
define OPENSSL_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
