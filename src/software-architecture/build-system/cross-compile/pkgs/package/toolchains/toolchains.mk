PKG = TOOLCHAINS
TOOLCHAINS_VERSION = 11.2-2022.02
TOOLCHAINS_SITE = https://developer.arm.com/-/media/Files/downloads/gnu/$(TOOLCHAINS_VERSION)/binrel

PKG_NAME = $(notdir $(CURDIR))
TARFILE_NAME = gcc-arm-$(TOOLCHAINS_VERSION)-x86_64-aarch64-none-linux-gnu
TARFILE_TYPE = tar.xz
PKG_TARFILE = $(TARFILE_NAME).$(TARFILE_TYPE)

# 1. Download source code
LOCAL_TARFILE = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_TARFILE))
ifneq ($(LOCAL_TARFILE),)
define TOOLCHAINS_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)tar -xf $(COM_EXTERNAL_DIR)/$(PKG_TARFILE) -C $(BUILD_DIR)/$(PKG_NAME)
endef
else
define TOOLCHAINS_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)cd $(DOWNLOAD_DIR) && wget $(TOOLCHAINS_SITE)/$(PKG_TARFILE)
	$(Q)tar -xf $(DOWNLOAD_DIR)/$(PKG_TARFILE) -C $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define TOOLCHAINS_PATCH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# 3. Compile source code
DEB_FILES = $(wildcard $(INSTALL_DIR)/$(PKG_NAME)/*deb)
define TOOLCHAINS_COMPILE_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# 4. Install compiled binaries
define TOOLCHAINS_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && find . -type f -exec sudo install -D {} $(INSTALL_DIR)/{} \;
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && find . -type l -exec sudo cp -d {} $(INSTALL_DIR)/{} \;
endef

# 5. Do closing work before finish
define TOOLCHAINS_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
