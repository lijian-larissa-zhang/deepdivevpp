PKG = VPP
VPP_VERSION = 2202
VPP_SITE = https://github.com/FDio/vpp.git

PKG_NAME = $(notdir $(CURDIR))

# 1. Download source code
LOCAL_SRC = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_NAME)*)
ifeq ($(LOCAL_SRC),)
define VPP_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone -b stable/$($(PKG)_VERSION) $($(PKG)_SITE) $(BUILD_DIR)/$(PKG_NAME)
endef
else
define VPP_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone $(LOCAL_SRC) $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
ifeq ($(CROSS),1)
PATCH_FILES += $(wildcard $(PACKAGE_DIR)/$(PKG_NAME)/cross-compile/*patch)
endif
define VPP_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
ifeq ($(CROSS),1)
define VPP_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && \
		PATH=$(PATH):$(ENV_PATH) \
		EXTRA_INSTALL_PATH=$(INSTALL_DIR) \
		VPP_EXTRA_CMAKE_ARGS="-DCMAKE_LIBRARY_PATH=$(INSTALL_DIR)/lib -DCMAKE_TOOLCHAIN_FILE=$(BUILD_DIR)/$(PKG_NAME)/src/cmake/aarch64_toolchain.cmake -DVPP_EXTRA_INSTALL_PATH=$(INSTALL_DIR)" \
		make build-release UNATTENDED=y
endef
else
define VPP_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && $(Q)make install-deps UNATTENDED=y
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && $(Q)make pkg-deb
endef
endif

# 4. Install compiled binaries
DEB_FILES = $(wildcard $(BUILD_DIR)/$(PKG_NAME)/build-root/*deb)

ifeq ($(CROSS),1)
define VPP_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build-root/install-vpp-native/vpp && \
		find . -type f -exec sudo install -D {} $(INSTALL_DIR)/{} \;
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build-root/install-vpp-native/vpp && \
		find . -type l -exec sudo cp -d {} $(INSTALL_DIR)/{} \;;
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build-root/install-vpp-native/external && \
		find . -type f -exec sudo install -D {} $(INSTALL_DIR)/{} \;
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build-root/install-vpp-native/external && \
		find . -type l -exec sudo cp -d {} $(INSTALL_DIR)/{} \;;
endef
else
define VPP_INSTALL_CMDS
	$(foreach f, $(sort $(DEB_FILES)), cd $(BUILD_DIR)/$(PKG_NAME)/build-root ; sudo dpkg-deb -x $(f) $(INSTALL_DIR) ;)
endef
endif

# 5. Do closing work before finish
define VPP_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
