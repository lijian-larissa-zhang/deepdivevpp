PKG = NUMACTL
NUMACTL_VERSION = v2.0.13
NUMACTL_SITE = https://github.com/numactl/numactl.git

PKG_NAME = $(notdir $(CURDIR))

# 1. Download source code
LOCAL_SRC = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_NAME)*)
ifeq ($(LOCAL_SRC),)
define NUMACTL_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone -b $($(PKG)_VERSION) $($(PKG)_SITE) $(BUILD_DIR)/$(PKG_NAME)
endef
else
define NUMACTL_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone $(LOCAL_SRC) $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define NUMACTL_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define NUMACTL_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && ./autogen.sh && autoconf -i
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && PATH=$(PATH):$(ENV_PATH) ./configure --host=aarch64-linux-gnu CC=aarch64-none-linux-gnu-gcc --prefix=$(INSTALL_DIR)
endef

# 4. Install compiled binaries
define NUMACTL_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && sudo PATH=$(PATH):$(ENV_PATH) make install
endef

# 5. Do closing work before finish
define NUMACTL_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
