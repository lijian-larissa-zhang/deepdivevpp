PKG = LIBBPF
LIBBPF_VERSION = v0.8.1
LIBBPF_SITE = https://github.com/libbpf/libbpf.git

PKG_NAME = $(notdir $(CURDIR))

# 1. Download source code
LOCAL_SRC = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_NAME)*)
ifeq ($(LOCAL_SRC),)
define LIBBPF_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone -b $($(PKG)_VERSION) $($(PKG)_SITE) $(BUILD_DIR)/$(PKG_NAME)
endef
else
define LIBBPF_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone $(LOCAL_SRC) $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define LIBBPF_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define LIBBPF_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && cd src && \
		sudo \
		EXTRA_CFLAGS=-I$(INSTALL_DIR)/include \
		EXTRA_LDFLAGS=-L$(INSTALL_DIR)/lib \
		PATH=$(PATH):$(ENV_PATH) \
		DESTDIR=$(INSTALL_DIR) \
		make install
endef

# 4. Install compiled binaries
define LIBBPF_INSTALL_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# 5. Do closing work before finish
define LIBBPF_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
