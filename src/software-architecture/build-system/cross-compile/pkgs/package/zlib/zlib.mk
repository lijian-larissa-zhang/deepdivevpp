PKG = ZLIB
ZLIB_VERSION = 1.2.12
ZLIB_SITE = https://zlib.net
PKG_NAME = $(notdir $(CURDIR))
TARFILE_NAME = $(PKG_NAME)-$(ZLIB_VERSION)

# 1. Download source code
define ZLIB_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && wget $(ZLIB_SITE)/$(TARFILE_NAME).tar.xz
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && tar -xf $(TARFILE_NAME).tar.xz
endef

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define ZLIB_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define ZLIB_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && \
		PATH=$(PATH):$(ENV_PATH) \
		CC=aarch64-none-linux-gnu-gcc \
		cc=aarch64-none-linux-gnu-gcc \
		./configure --prefix=$(INSTALL_DIR)
endef

# 4. Install compiled binaries
define ZLIB_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && sudo PATH=$(PATH):$(ENV_PATH) make install
endef

# 5. Do closing work before finish
define ZLIB_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
