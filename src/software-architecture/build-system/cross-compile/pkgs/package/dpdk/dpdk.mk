PKG = DPDK
DPDK_VERSION = 21.11
DPDK_SITE = https://dpdk.org/git/dpdk-stable

PKG_NAME = $(notdir $(CURDIR))

# 1. Download source code
LOCAL_SRC = $(wildcard $(COM_EXTERNAL_DIR)/$(PKG_NAME)*)
ifeq ($(LOCAL_SRC),)
define DPDK_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone -b $($(PKG)_VERSION) $($(PKG)_SITE) $(BUILD_DIR)/$(PKG_NAME)
endef
else
define DPDK_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)git clone $(LOCAL_SRC) $(BUILD_DIR)/$(PKG_NAME)
endef
endif

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define DPDK_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME) ; git am $(f) ;)
endef

# 3. Compile source code
ifeq ($(CROSS),1)
CROSSFILE = arm64_armv8_linux_gcc
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define DPDK_COMPILE_CMDS
	$(Q)sed -i 's/<install_dir>/$(subst /,\/,$(INSTALL_DIR))/g' $(BUILD_DIR)/$(PKG_NAME)/config/arm/$(CROSSFILE)
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && PATH=$(PATH):$(ENV_PATH) meson $(BUILD_DIR)/$(PKG_NAME)/build \
		--cross-file $(BUILD_DIR)/$(PKG_NAME)/config/arm/$(CROSSFILE) \
		-Dexamples=all -Ddisable_drivers=raw/ifpga
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build && PATH=$(PATH):$(ENV_PATH) ninja
endef
else
define DPDK_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && $(Q)meson $(BUILD_DIR)/$(PKG_NAME)/build -Dexamples=all -Ddisable_drivers=raw/ifpga
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build && $(Q)ninja
endef
endif

# 4. Install compiled binaries
define DPDK_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/build && $(Q)sudo DESTDIR=$(INSTALL_DIR) ninja install
	$(Q)sudo install -m 755 $(BUILD_DIR)/dpdk/build/examples/dpdk-* $(INSTALL_DIR)/usr/local/bin/
endef

# 5. Do closing work before finish
define DPDK_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
