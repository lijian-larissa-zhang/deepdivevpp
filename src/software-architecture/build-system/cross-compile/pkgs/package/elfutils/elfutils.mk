PKG = ELFUTILS
ELFUTILS_VERSION = 0.181
ELFUTILS_SITE = https://sourceware.org/elfutils/ftp
PKG_NAME = $(notdir $(CURDIR))
TARFILE_NAME = $(PKG_NAME)-$(ELFUTILS_VERSION)

# 1. Download source code
define ELFUTILS_DOWNLOAD_CMDS
	$(Q)mkdir -p $(BUILD_DIR)/$(PKG_NAME)
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && wget $(ELFUTILS_SITE)/$($(PKG)_VERSION)/$(TARFILE_NAME).tar.bz2
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME) && tar -xf $(TARFILE_NAME).tar.bz2
endef

# 2. Apply patches to source code
PATCH_DIR = $(PACKAGE_DIR)/$(PKG_NAME)/$($(PKG)_VERSION)
PATCH_FILES = $(wildcard $(PATCH_DIR)/*patch)
define ELFUTILS_PATCH_CMDS
 	$(foreach f, $(sort $(PATCH_FILES)), $(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) ; patch -p1 < $(f) ;)
endef

# 3. Compile source code
ENV_PATH = $(INSTALL_DIR)/bin:$(INSTALL_DIR)/usr/bin:$(INSTALL_DIR)/usr/local/bin
define ELFUTILS_COMPILE_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && \
		LIBS="-lz" \
		CFLAGS=-I$(INSTALL_DIR)/include \
		LDFLAGS=-L$(INSTALL_DIR)/lib \
		CC=aarch64-none-linux-gnu-gcc \
		PATH=$(PATH):$(ENV_PATH) \
		./configure \
		--disable-libdebuginfod \
		--disable-debuginfod \
		--host=aarch64-linux-gnu \
		--prefix=$(INSTALL_DIR)
endef

# 4. Install compiled binaries
define ELFUTILS_INSTALL_CMDS
	$(Q)cd $(BUILD_DIR)/$(PKG_NAME)/$(TARFILE_NAME) && \
		sudo \
		LIBS="-lz" \
		CFLAGS=-I$(INSTALL_DIR)/include \
		LDFLAGS=-L$(INSTALL_DIR)/lib \
		CC=aarch64-none-linux-gnu-gcc \
		PATH=$(PATH):$(ENV_PATH) \
		make install
endef

# 5. Do closing work before finish
define ELFUTILS_FINISH_CMDS
	$(Q)echo "Nothing to do in this step"
endef

# Generic routines
include $(COM_DIR)/pkg-generic.mk
