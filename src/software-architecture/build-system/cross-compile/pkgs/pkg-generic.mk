qstrip = $(strip $(subst ",,$(1)))
TERM_BOLD := $(shell tput smso 2>/dev/null)
TERM_RESET := $(shell tput rmso 2>/dev/null)
MESSAGE = echo "$(TERM_BOLD)>>> $(PKG) $($(PKG)_VERSION) $(call qstrip,$(1))$(TERM_RESET)"

.PRECIOUS: $(BUILD_DIR)/%/.stamp_downloaded
.PRECIOUS: $(BUILD_DIR)/%/.stamp_patched
.PRECIOUS: $(BUILD_DIR)/%/.stamp_compiled
.PRECIOUS: $(BUILD_DIR)/%/.stamp_installed
.PRECIOUS: $(BUILD_DIR)/%/.stamp_finished

$(BUILD_DIR)/%/.stamp_downloaded:
	@$(call MESSAGE,"Downloading source code")
	+$($(PKG)_DOWNLOAD_CMDS)
	$(Q)touch $@

$(BUILD_DIR)/%/.stamp_patched: $(BUILD_DIR)/%/.stamp_downloaded
	@$(call MESSAGE,"Applying patches")
	+$($(PKG)_PATCH_CMDS)
	$(Q)touch $@

$(BUILD_DIR)/%/.stamp_compiled: $(BUILD_DIR)/%/.stamp_patched
	@$(call MESSAGE,"Compiling source code")
	+$($(PKG)_COMPILE_CMDS)
	$(Q)touch $@

$(BUILD_DIR)/%/.stamp_installed: $(BUILD_DIR)/%/.stamp_compiled
	@$(call MESSAGE,"Intalling compiled binaries")
	+$($(PKG)_INSTALL_CMDS)
	$(Q)touch $@

$(BUILD_DIR)/%/.stamp_finished: $(BUILD_DIR)/%/.stamp_installed
	@$(call MESSAGE,"Closing processes")
	+$($(PKG)_FINISH_CMDS)
	$(Q)touch $@

%: $(BUILD_DIR)/%/.stamp_finished
	@$(call MESSAGE,"Finished")

.PHONY: %-clean
%-clean:
	$(Q)rm -rf $(BUILD_DIR)/$(strip $(subst -clean,,$@))

