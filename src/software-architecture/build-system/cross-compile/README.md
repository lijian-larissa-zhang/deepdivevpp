1. ``make all`` builds DPDK and VPP images.
2. ``make dpdk TARGET_MACHINE=aarch64`` cross-compiles DPDK for aarch64 target.
3. ``make vpp TARGET_MACHINE=aarch64`` cross-compiles VPP for aarch64 target.
4. ``make clean``, ``make dpdk-clean``, ``make vpp-clean`` clears all generated files.