VENV_DIR      ?= doc/venv
PYTHON        ?= python3

.PHONY: venv
venv:
	@( \
	    if [ ! -d ${VENV_DIR} ]; then \
	      ${PYTHON} -m venv ${VENV_DIR}; \
	      . ${VENV_DIR}/bin/activate; \
	      ${PYTHON} -m pip install -r doc/requirements.txt; \
	    fi; \
	)

.PHONY: doc
doc: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    make html; \
	)

.PHONY: doc-clean
doc-clean: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    make clean; \
	)

.PHONY: help
help:
	@echo "Make Targets:"
	@echo " doc                  - Build the Sphinx documentation"
	@echo " doc-clean            - Remove the generated files from the Sphinx docs"
