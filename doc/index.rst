.. Deep Dive VPP documentation master file, created by
   sphinx-quickstart on Tue May 24 22:58:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

深入浅出VPP
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   agreement
   toc
   overview/index
   software-architecture/index
   network-function/index
   application/index
   meeting-minutes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
