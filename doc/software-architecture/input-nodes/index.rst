
网络接口类型
==================

本部分介绍VPP支持的各种接口类型及其实现细节，包括DPDK、memif、AF_Packet、AVF、RDMA、AF_XDP等。

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   AVF