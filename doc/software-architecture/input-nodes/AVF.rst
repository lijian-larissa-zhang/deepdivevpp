AVF设备驱动
=======================

概述
--------

本插件是一个虚拟适配网卡功能，Adaptive Virtual Function (AVF) [1]_ [2]_，只有较新种类的intel网卡才能支持这个虚拟功能（目前功能支持intel XL710 / X710 / XXV710 / V710 / X722 / E810 等适配器） [3]_。

功能介绍
--------------

AVF定义了一个虚拟设备驱动规范，规范里构建了一个物理网卡和虚拟设备的信息交互通路，基于物理网卡虚拟出一个linux设备驱动。该虚拟设备可以适配多种不同的intel以太网控制器，该设备对系统统一呈现一个设备ID（8086：1889），所以使用该虚拟适配网卡的应用无需再关注物理的硬件设备的更新，虚拟设备提供一个基本功能集合，并支持更多功能的接口后续添加。

但需要注意的是：虚拟资源基于物理资源基础上存在，有使用的前提条件，特殊的功能需要在使用前确认硬件网卡是否支持。VPP的AVF插件实现相当于在用户态重构了iavf的内核驱动实现，只针对有限版本的网卡进行了验证，插件驱动版本只对 XL710 / X710 / XXV710 使用，更多的支持需要自己额外验证。

.. important::
  为了进一步规范功能的实现，有以下使用前提
    - linux系统需要安装使用比较新的i40e PF linux驱动程序，支持virtualchnl接口。建议使用i40e PF linux驱动程序版本2.4.6进行测试。
    - 驱动程序需要 MSI-X 中断功能来支持，因此系统需要支持使用 vfio-pci驱动程序。并建议在IOMUU功能下进行测试。

实现背景
--------------

从云化应用来看，需要硬件设备能够提供一套这样的理想I/O虚拟化设备框架。在这个框架下，即能使设备保留高性能的同时，又能具备各种复杂的云化特性。传统的非云化设备遵循此框架，理论上可以做到无缝向云化扩展。

Intel IAVF作为第一个向云化扩展的具现例子，应用基于高性能场景的网卡硬件基础上，使用IAVF方式更可以增加设备的使用灵活性。VPP的AVF接口接口近似将设备的直接使用，使用VFIO这个设备直通框架将设备直接分配给用户态应用使用，由于设备直接分配天然地就让设备数据面直通而达到了很高的性能，在性能方面也能满足要求。

如上描述VPP的AVF功能是VPP原生用户态的驱动设备实现，不需要其他的支持（如DPDK，DPDK也支持对IAVF的PMD，但是从基于DPDK的框架的使用和VPP的AVF直接使用对比，DPDK的就相对臃肿，VPP的AVF更直观些。实际的应用中，并不需要太多的功能，DPDK本身提供一个平台适配，框架中本身会携带很多网卡无关特性，并兼容其他网卡功能，封装了ETHDEV作为通用的设备抽象层。）就可以使用。驱动接口直接在VPP的框架下实现，可以适配多款系列硬件，减轻代码维护过程中对硬件专有驱动的更新负担，有助于提升高质量数据处理的体验。可以提供接口，动态的调整应用的数据定向处理，实现应用的对特殊业务的可控服务。从大的方面来说，简化了硬件更新对配套硬件的开发过程，缩短了网络功能部署的周期。

实现机制
--------------

Intel定义了一套PF和VF之间的通信机制（a virtual channel protocol）实现信息的传递。AVF设备作为一个从属驱动，它的初始化需要几秒的时间开销，用于和宿主设备通信交互来执行设备的初始化。

从流程上大致分成两个步骤，首先需要完成网络设备在操作系统中注册，并完成相应的设备初始化和加载操作。其次，需要将初始化配置的操作，并推送到信息通道上。具体的操作步骤可以参见文档 [3]_ 中第六章节的描述。

代码文件
~~~~~~~~~~~~~~

src/plugins/avf ::

	├── avf_all_api_h.h
	├── avf.api
	├── avf_api.c
	├── avf.h
	├── avf_msg_enum.h
	├── avf_test.c
	├── cli.c
	├── CMakeLists.txt
	├── device.c
	├── format.c
	├── input.c
	├── output.c
	├── plugin.c
	├── README.md
	└── virtchnl.h

代码分析
~~~~~~~~~~~~~~~~

AVF通过调用CLI命令接口，可以完成指定设备的创建、配置和初始化工作以及删除设备接口。
对VPP提供以下的硬件接口封装操作

.. code-block:: c

	/* *INDENT-OFF* */
	VNET_DEVICE_CLASS (avf_device_class, ) = {
	  .name = "Adaptive Virtual Function (AVF) interface",   //设备名称
	  .clear_counters = avf_clear_hw_interface_counters,     //清除统计
	  .format_device = format_avf_device,
	  .format_device_name = format_avf_device_name,
	  .admin_up_down_function = avf_interface_admin_up_down, //设备的up/down配置操作接口函数
	  .rx_mode_change_function = avf_interface_rx_mode_change,//接收模式变更接口
	  .rx_redirect_to_node = avf_set_interface_next_node,//强制重定向接口流量到指定的node节点
	  .mac_addr_add_del_function = avf_add_del_mac_address,//变更链路层地址函数接口
	  .tx_function_n_errors = AVF_TX_N_ERROR,   //发送错误码数据大小
	  .tx_function_error_strings = avf_tx_func_error_strings,//发送错误码映射字符串
	  .flow_ops_function = avf_flow_ops_fn,//流量卸载操作接口
	};
	/* *INDENT-ON* */

AVF发送的接口，在output.c定义描述

.. code-block:: c

	VNET_DEVICE_CLASS_TX_FN (avf_device_class) (vlib_main_t * vm,
						    vlib_node_runtime_t * node,
						    vlib_frame_t * frame)

AVF的数据接收处理节点，在input.c定义描述

.. code-block:: c

	/* *INDENT-OFF* */
	VLIB_REGISTER_NODE (avf_input_node) = {
	  .name = "avf-input",
	  .sibling_of = "device-input",
	  .format_trace = format_avf_input_trace,
	  .type = VLIB_NODE_TYPE_INPUT,
	  .state = VLIB_NODE_STATE_DISABLED,
	  .n_errors = AVF_INPUT_N_ERROR,//接收错误码数据大小
	  .error_strings = avf_input_error_strings,//接收错误码映射字符串
	  .flags = VLIB_NODE_FLAG_TRACE_SUPPORTED,//支持节点的trace跟踪
	};

	/* *INDENT-ON* */

AVF的事件处理节点，受理设备删除、中断、网口状态等事件处理

.. code-block:: c

	/* *INDENT-OFF* */
	VLIB_REGISTER_NODE (avf_process_node)  = {
	  .function = avf_process,
	  .type = VLIB_NODE_TYPE_PROCESS,
	  .name = "avf-process",
	};
	/* *INDENT-ON* */

AVF的配置都是通过virtchnl间接先发送到PF，完成期望的操作执行。每一个OP的面临参数结构有单独的定义解释。

.. code-block:: c

	clib_error_t *
	avf_send_to_pf (vlib_main_t * vm, avf_device_t * ad, virtchnl_ops_t op,
			void *in, int in_len, void *out, int out_len)

OP操作的相关定义描述参见virtchnl.h的virtchnl_ops_t定义描述，VPP的配置过程中目前只使用了

========================================         ================================
VIRTCHNL_OP_VERSION                               OP版本，设置为1     
VIRTCHNL_OP_GET_VF_RESOURCES                      获取VF资源
VIRTCHNL_OP_CONFIG_RSS_LUT                        Receive Side Scaling lookup table
VIRTCHNL_OP_CONFIG_RSS_KEY                        配置RSS KEY  
VIRTCHNL_OP_DISABLE_VLAN_STRIPPING                关闭VLAN STRIP
VIRTCHNL_OP_CONFIG_PROMISCUOUS_MODE               配饰端口混杂模式
VIRTCHNL_OP_CONFIG_VSI_QUEUES                     VSI的队列配置
VIRTCHNL_OP_CONFIG_IRQ_MAP                        配置中断向量表
VIRTCHNL_OP_ADD_ETH_ADDR                          添加链路层地址
VIRTCHNL_OP_DEL_ETH_ADDR                          删除链路层地址
VIRTCHNL_OP_ENABLE_QUEUES                         使能队列
VIRTCHNL_OP_GET_STATS                             获取链路层统计
VIRTCHNL_OP_GET_OFFLOAD_VLAN_V2_CAPS              获取OFFLOAD配置
VIRTCHNL_OP_DISABLE_VLAN_STRIPPING_V2             关闭VLAN STRIP
VIRTCHNL_OP_RESET_VF                              VF复位
VIRTCHNL_OP_REQUEST_QUEUES                        queue 规格配置请求
VIRTCHNL_OP_ADD_FDIR_FILTER                       添加流控ACL配置
VIRTCHNL_OP_DEL_FDIR_FILTER                       删除流控ACL配置
========================================         ================================


配置和使用方法
------------------

linux系统配置
~~~~~~~~~~~~~~~~~~~~

1. 加载vfio-pci驱动程序
::

   sudo modprobe vfio-pci

2. 如果系统不支持IOMMU，需要进行如下配置
::

   echo Y | sudo tee /sys/module/vfio/parameters/enable_unsafe_noiommu_mode

3. 创建和捆绑SRIOV的虚拟设备
以下是VPP说明文档中提供的一个过程脚本，实现的功能和DPDK网口的接管过程比较类似。将设备从系统管理里去关，用户态直接管理。VF设备建议配置静态MAC，否则初始化启动后是随机值生成配置状态。

.. code:: bash

   #!/bin/bash

   if [ $USER != "root" ] ; then                          #检查操作权限
       echo "Restarting script with sudo..."
       sudo $0 ${*}
       exit
   fi

   setup () {
     cd /sys/bus/pci/devices/${1}
     driver=$(basename $(readlink driver))
     if [ "${driver}" != "i40e" ]; then                   #i40e设备检测
       echo ${1} | tee driver/unbind                      #从系统内核下解绑
       echo ${1} | tee /sys/bus/pci/drivers/i40e/bind     #将设备绑定到PMD下
     fi
     ifname=$(basename net/*)
     echo 0 | tee sriov_numvfs > /dev/null                #清除之前的设置
     echo 1 | tee sriov_numvfs > /dev/null                #设置VF的个数为1个
     ip link set dev ${ifname} vf 0 mac ${2}              #配置VF的MAC地址
     ip link show dev ${ifname}                           #显示
     vf=$(basename $(readlink virtfn0))
     echo ${vf} | tee virtfn0/driver/unbind                       #将VF从系统内核下解绑
     echo vfio-pci | tee virtfn0/driver_override                  #添加vfio-pci关联到VF设备树
     echo ${vf} | sudo tee /sys/bus/pci/drivers/vfio-pci/bind     #VF绑定到VFIO-PCI
     echo  | tee virtfn0/driver_override                          #清除关系
   }

   # Setup one VF on PF 0000:3b:00.0 and assign MAC address
   setup 0000:3b:00.0 00:11:22:33:44:00
   # Setup one VF on PF 0000:3b:00.1 and assign MAC address
   setup 0000:3b:00.1 00:11:22:33:44:01


4. 混杂模式配置，系统默认关闭状态。在一些场景下使用可能需要打开混杂配置。可以使用linux系统ip link命令配置。
::

   ip link set dev <PF inteface name> vf <VF id> trust on

5. 关闭L2的spoofing检测，系统默认打开状态，对于发送出的数据帧的SMAC不等于VF的配置MAC，会对此类数据帧丢弃处理。在一些应用场景（比如 L2 bridging, bonding, l2 cross-connect），不能确保VPP发送出的数据帧的L2的SMAC一定等于AVF端口的配置MAC，为了保证数据帧能正常送出，需要关闭系统的spoofing的检测。可以使用linux系统ip link命令配置。
::

   ip link set dev <PF inteface name> vf <VF id> spoofchk off

命令介绍
~~~~~~~~~~~~~~~~~~~~

创建一个avf接口
::

    create interface avf <pci-address> [rx-queue-size <size>] [tx-queue-size <size>] [num-rx-queues <size>]

删除一个avf接口
::

    delete interface avf {<interface> | sw_if_index <sw_idx>}

测试avf接口
::

    test avf [<interface> | sw_if_index <sw_idx>] [irq] [elog-on] [elog-off]


参考
--------
...


.. [1] https://s3-docs.fd.io/vpp/22.02/developer/devicedrivers/index.html
.. [2] https://doc.dpdk.org/guides-22.03/nics/intel_vf.html
.. [3] https://www.intel.com/content/dam/www/public/us/en/documents/product-specifications/ethernet-adaptive-virtual-function-hardware-spec.pdf
