网络调试 - Trace
=====================

做网络开发和调试肯定少不了抓包这个手段，抓包可以从最基础的报文结构和改变来分析程序的逻辑和问题。
VPP提供了trace功能来进行抓包及观察报文被每个node节点处理的全生命周期。

功能介绍
--------------

trace功能只能从VLIB_NODE_TYPE_INPUT节点开始，如dpdk-input即为起点，报文从dpdk-input节点进入，才会进入trace，还可以被指定的input节点有af-packet-input或vhost-user-input等等

使用方法
--------------

**基本用法**

.. code-block:: console

    trace add <input-graph-node> <numbers> [filter] [verbose]

显示结果：

.. code-block:: console

    show trace

例子：

.. code-block:: console

    root@test:~# vppctl show int addr
    GigabitEthernet0/8/0 (up):
    L3 192.168.20.10/24
    local0 (dn):

    root@test:~# vppctl trace add dpdk-input 100
    root@test:~# vppctl show trace

    Limiting display to 50 packets. To display more specify max.
    ------------------- Start of thread 0 vpp_main -------------------
    Packet 1

    00:04:47:510540: dpdk-input
    GigabitEthernet0/8/0 rx queue 0
    buffer 0x99b96: current data 0, length 86, buffer-pool 0, ref-count 1, trace handle 0x0
                    ext-hdr-valid
    PKT MBUF: port 0, nb_segs 1, pkt_len 86
        buf_len 2176, data_len 86, ol_flags 0x0, data_off 128, phys_addr 0x7166e600
        packet_type 0x0 l2_len 0 l3_len 0 outer_l2_len 0 outer_l3_len 0
        rss 0x0 fdir.hi 0x0 fdir.lo 0x0
    IP6: 00:e0:4c:f0:4b:8c -> 33:33:ff:00:00:00
    ICMP6: fe80::2e0:4cff:fef0:4b8c -> ff02::1:ff00:0
        tos 0x00, flow label 0x0, hop limit 255, payload length 32
    00:04:47:510659: ethernet-input
    frame: flags 0x3, hw-if-index 1, sw-if-index 1
    IP6: 00:e0:4c:f0:4b:8c -> 33:33:ff:00:00:00
    00:04:47:510668: ip6-input
    ICMP6: fe80::2e0:4cff:fef0:4b8c -> ff02::1:ff00:0
        tos 0x00, flow label 0x0, hop limit 255, payload length 32
    00:04:47:510674: ip6-not-enabled
        fib:0 adj:0 flow:0
    ICMP6: fe80::2e0:4cff:fef0:4b8c -> ff02::1:ff00:0
        tos 0x00, flow label 0x0, hop limit 255, payload length 32
    00:04:47:510681: error-drop
    rx:GigabitEthernet0/8/0
    00:04:47:510684: drop
    dpdk-input: no error

    ...（此处省略若干个报文）

    Packet 50

    00:04:51:444794: dpdk-input
    GigabitEthernet0/8/0 rx queue 0
    buffer 0x9accd: current data 0, length 98, buffer-pool 0, ref-count 1, trace handle 0x31
                    ext-hdr-valid
    PKT MBUF: port 0, nb_segs 1, pkt_len 98
        buf_len 2176, data_len 98, ol_flags 0x0, data_off 128, phys_addr 0x716b33c0
        packet_type 0x0 l2_len 0 l3_len 0 outer_l2_len 0 outer_l3_len 0
        rss 0x0 fdir.hi 0x0 fdir.lo 0x0
    IP4: 08:00:27:ad:98:6c -> 08:00:27:c3:ba:7d
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
    ICMP echo_request checksum 0x6992 id 595
    00:04:51:444852: ethernet-input
    frame: flags 0x3, hw-if-index 1, sw-if-index 1
    IP4: 08:00:27:ad:98:6c -> 08:00:27:c3:ba:7d
    00:04:51:444858: ip4-input-no-checksum
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
    ICMP echo_request checksum 0x6992 id 595
    00:04:51:444861: ip4-lookup
    fib 0 dpo-idx 7 flow hash: 0x00000000
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
    ICMP echo_request checksum 0x6992 id 595
    00:04:51:444864: ip4-receive
        ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
        ICMP echo_request checksum 0x6992 id 595
    00:04:51:444865: ip4-icmp-input
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
    ICMP echo_request checksum 0x6992 id 595
    00:04:51:444866: ip4-icmp-echo-request
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x05b4 dscp CS0 ecn NON_ECN
        fragment id 0x8b86, flags DONT_FRAGMENT
    ICMP echo_request checksum 0x6992 id 595
    00:04:51:444882: ip4-load-balance
    fib 0 dpo-idx 2 flow hash: 0x00000000
    ICMP: 192.168.20.10 -> 192.168.20.20
        tos 0x00, ttl 64, length 84, checksum 0xbc58 dscp CS0 ecn NON_ECN
        fragment id 0xd4e1, flags DONT_FRAGMENT
    ICMP echo_reply checksum 0x7192 id 595
    00:04:51:444883: ip4-rewrite
    tx_sw_if_index 1 dpo-idx 2 : ipv4 via 192.168.20.20 GigabitEthernet0/8/0: mtu:8996 next:3 flags:[] 080027ad986c080027c3ba7d0800 flow hash: 0x00000000
    00000000: 080027ad986c080027c3ba7d080045000054d4e140004001bc58c0a8140ac0a8
    00000020: 14140000719202530021d44afb6200000000f5780800000000001011
    00:04:51:444885: GigabitEthernet0/8/0-output
    GigabitEthernet0/8/0
    IP4: 08:00:27:c3:ba:7d -> 08:00:27:ad:98:6c
    ICMP: 192.168.20.10 -> 192.168.20.20
        tos 0x00, ttl 64, length 84, checksum 0xbc58 dscp CS0 ecn NON_ECN
        fragment id 0xd4e1, flags DONT_FRAGMENT
    ICMP echo_reply checksum 0x7192 id 595
    00:04:51:444887: GigabitEthernet0/8/0-tx
    GigabitEthernet0/8/0 tx queue 0
    buffer 0x9accd: current data 0, length 98, buffer-pool 0, ref-count 1, trace handle 0x31
                    ext-hdr-valid
                    local l2-hdr-offset 0 l3-hdr-offset 14
    PKT MBUF: port 0, nb_segs 1, pkt_len 98
        buf_len 2176, data_len 98, ol_flags 0x0, data_off 128, phys_addr 0x716b33c0
        packet_type 0x0 l2_len 0 l3_len 0 outer_l2_len 0 outer_l3_len 0
        rss 0x0 fdir.hi 0x0 fdir.lo 0x0
    IP4: 08:00:27:c3:ba:7d -> 08:00:27:ad:98:6c
    ICMP: 192.168.20.10 -> 192.168.20.20
        tos 0x00, ttl 64, length 84, checksum 0xbc58 dscp CS0 ecn NON_ECN
        fragment id 0xd4e1, flags DONT_FRAGMENT
    ICMP echo_reply checksum 0x7192 id 595

    Limiting display to 50 packets. To display more specify max.

实现基本流程
--------------

以从dpdk-input进入为例：

当使用trace add dpdk-input 100后，数据结构(vlib_trace_node_t)(dpdk)->limit就会增加

在dpdk模块的node处理节点函数中dpdk_device_input有调用vlib_get_trace_count（获取剩余需抓包数量，此处即返回上面增加的抓包数）

然后在dpdk_device_input中还会调用vlib_trace_buffer（判断报文是否符合抓取条件），符合后对报文做标记b->flags |= VLIB_BUFFER_IS_TRACED，并使用pool_get (tm->trace_buffer_pool, h)申请一个trace信息链缓存，后续所有node对这个报文的处理信息都将添加到这个trace信息链缓存中

比如nat模块的in2out

.. code-block:: c

    if (PREDICT_FALSE
	  ((node->flags & VLIB_NODE_FLAG_TRACE)
	   && (b0->flags & VLIB_BUFFER_IS_TRACED)))
	{
	  nat_in2out_ed_trace_t *t =
	    vlib_add_trace (vm, node, b0, sizeof (*t));
	  t->sw_if_index = rx_sw_if_index0;
      ...
	}

vlib_add_trace的流程，把该node的trace信息结构添加到报文trace信息链缓存中

vlib_add_trace逻辑：获取trace_index(报文在trace_buffer_pool中的索引)，然后计算当前node调用vlib_add_trace传入的trace结构大小与trace_header_t对齐的长度，trace_header_t的长度为16字节，假如传入的trace结构体大小为31字节，那么

.. code-block:: c

    n_data_bytes = round_pow2 (n_data_bytes, sizeof (h[0]));        这里计算后为32
    n_data_words = n_data_bytes / sizeof (h[0]);                    这里计算后为需要2个trace_header_t大小的长度
    vec_add2_aligned (tm->trace_buffer_pool[trace_index], h, 1 + n_data_words,
                sizeof (h[0]));                                     最后添加1*header+n*header大小长度的data数据

完成所有抓包后，在show trace的时候，调用cli_show_trace_buffer

该函数遍历所有trace信息缓存并打印，在每个trace信息中，都包含多个node对一个报文的处理过程，其中使用format_vlib_trace来打印具体的trace信息，在format_vlib_trace中，会遍历这个trace信息中的所有node trace信息结构，并使用node自己的trace格式打印

通过foreach in trace_buffer_pool中取出每个报文的*trace_header,
*trace_header指向了这个报文被所有处理节点node的trace信息结构，通过vec_header取得首个node添加的trace信息，通过vec_end取得末个node添加的tracer信息，并把第一到最后依次输出，通过h->data获取每个node传入的内容，通过node->format_trace使用node的trace格式输出

每个节点注册时指定format-trace，如：

.. code-block:: c

    VLIB_REGISTER_NODE (ethernet_input_node) = {
    .name = "ethernet-input",
    ...
    .format_buffer = format_ethernet_header_with_length,
    .format_trace = format_ethernet_input_trace,
    .unformat_buffer = unformat_ethernet_header,
    };
    static u8 *
    format_ethernet_input_trace (u8 * s, va_list * va)
    {
        CLIB_UNUSED (vlib_main_t * vm) = va_arg (*va, vlib_main_t *);
        CLIB_UNUSED (vlib_node_t * node) = va_arg (*va, vlib_node_t *);
        ethernet_input_trace_t *t = va_arg (*va, ethernet_input_trace_t *);
        u32 indent = format_get_indent (s);
        ...
        s = format (s, "%U", format_ethernet_header, t->packet_data);
        return s;
    }

之后在show trace的时候，会在format_vlib_trace中使用

.. code-block:: c

    if (node->format_trace)
		s = format (s, "\n  %U", node->format_trace, vm, node, h->data);



使用classify机制设定抓包过滤条件
--------------

**classify filter**

.. code-block:: console

    classify filter <intfc> | pcap mask <mask-value> match <match-value> | trace mask <mask-value> match <match-value> [del] [buckets <nn>] [memory-size <n>]

使用classify filter设定抓包过滤条件时，会首先根据mask条件创建一个classify table，再根据match添加session

然后把vlib_global_main.trace_filter.trace_filter_enable设为1

在vlib_trace_buffer中判断这个标记被设定的话，就进入vnet_is_packet_traced函数，
vnet_is_packet_traced函数的流程为在table中查找是否有和报文相匹配的session

例子：

.. code-block:: console

    root@test:~# vppctl classify filter trace mask l3 ip4 src dst match l3 ip4 src 192.168.20.20 dst 192.168.20.10
    root@test:~# vppctl trace add dpdk-input filter
    root@test:~# vppctl show trace

    root@test:~# vppctl show trace | grep -A 2 IP4
    IP4: 08:00:27:ad:98:6c -> 08:00:27:c3:ba:7d
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x7de0 dscp CS0 ecn NON_ECN
    --
    IP4: 08:00:27:ad:98:6c -> 08:00:27:c3:ba:7d
    00:09:32:408043: ip4-input-no-checksum
    ICMP: 192.168.20.20 -> 192.168.20.10
    --
    IP4: 08:00:27:c3:ba:7d -> 08:00:27:ad:98:6c
    ICMP: 192.168.20.10 -> 192.168.20.20
        tos 0x00, ttl 64, length 84, checksum 0x7185 dscp CS0 ecn NON_ECN
    --
    IP4: 08:00:27:c3:ba:7d -> 08:00:27:ad:98:6c
    ICMP: 192.168.20.10 -> 192.168.20.20
        tos 0x00, ttl 64, length 84, checksum 0x7185 dscp CS0 ecn NON_ECN
    --
    IP4: 08:00:27:ad:98:6c -> 08:00:27:c3:ba:7d
    ICMP: 192.168.20.20 -> 192.168.20.10
        tos 0x00, ttl 64, length 84, checksum 0x7ce8 dscp CS0 ecn NON_ECN

把抓取报文保存为pcap文件
--------------

**pcap dispatch trace**

.. code-block:: console

    pcap dispatch trace [on|off] [max <nn>] [file <name>] [status] [buffer-trace <input-node-name> <nn>][post-mortem]

函数vlib_pcap_dispatch_trace_configure会调用vlib_node_set_dispatch_wrapper然后设置vm->dispatch_wrapper_fn指向dispatch_pcap_trace

之后在dispatch_node的时候，都首先调用dispatch_pcap_trace再调用node本身的函数

dispatch_pcap_trace的逻辑

- 对于每一个报文，经过当前node的时候，都记录为一个pcap报文
- 首先构造pcap报文头，再把vlib_buffer基本信息拷贝
- 根据是否设置buffer-trace标记，把当前node的trace信息要拷贝进去
- 最后把vlib_buffer元数据拷贝进去
- 根据是否设置post-mortem，在工作线程每次循环前，都清空pcap缓存，重新开始构造pcap抓包记录，等到程序崩溃时，把所有在pcap中的缓存输出到文件

.. code-block:: c

    /* main thread only */
    此处为post-mortem的配置操作
  clib_callback_enable_disable (vm->worker_thread_main_loop_callbacks,
				vm->worker_thread_main_loop_callback_tmp,
				vm->worker_thread_main_loop_callback_lock,
				pcap_postmortem_reset, a->post_mortem);
  vlib_add_del_post_mortem_callback (pcap_postmortem_dump, a->post_mortem);

例子：

.. code-block:: console

    vppctl pcap dispatch trace on max 10000 file dispatchTrace.pcap   buffer-trace dpdk-input 1000
    vppctl pcap dispatch trace off

.. figure:: ../../images/trace/trace_pcap.png
   :align: center
