Feature arc机制
=======================

概述
--------------

VPP可以通过Feature机制灵活地控制node之间的连接。每个feature是一个node，用户可以启用或停止某个或某些feature，
来达到自己想要处理的逻辑。用户可以自己写插件，把自己定义的node（业务逻辑）加入的指定的位置。通过为接口指定
feature可以在接口上启用对应的feature功能。

在VPP中建立一个通用的机制来管理这些feature，具体来说就是将不同的feature按照业务逻辑分成了不同的组，每组feature称
之为一个arc。

在feature arc的开始，需要进行一些初始化的设置工作，前提是必须至少启用一个功能。在每个arc基础上，各个feature定义
创建一组排序依赖关系。arc中的feature按照代码指定的排序顺序串接起来。在arc结构中，记录这组feature中的起始node和结束node。
系统初始化时，根据用户设置node之间先后顺序完成初步的排序，但并没有应用到对应的接口中。

.. important::
    为了进一步规范功能的实现，feature arc使用需要注意一下几点 [1]_：
	- feature arc创建时，至少需要设置一个起始node节点。
	- arc 组中的feature需要保证能注册成功，否则使用可能导致异常。
	- 某些feature需要根据实际业务处理逻辑指定特定的顺序，顺序错乱可能会导致业务处理失败，具体可以参考[1]_中例子。
	- 不能存在循环关系，否则vpp将拒绝运行; ``a then b, b then c, c then a`` 形式的循环依赖循环是不可能满足的。

实现机制
--------------

feature arc 的使用分为四个步骤（参见文档 [2]_ 中的描述）：

	- 创建一个feature arc；
	- 将feature添加到feature arc上；
	- 启用或禁用feature功能；
	- 在feature业务处理逻辑中使用api将报文分发到一个feature节点上。

下面我们以 ``ip4_unicast`` 为例分析feature arc的处理逻辑：	

创建feature arc
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

我们使用构造函数宏 ``VNET_FEATURE_ARC_INIT`` 来创建 ``ip4_unicast`` arc，如下：

.. code-block:: c

	VNET_FEATURE_ARC_INIT (ip4_unicast, static) =
	{
		.arc_name = "ip4-unicast",
		.start_nodes = VNET_FEATURES ("ip4-input", "ip4-input-no-checksum"),
		.last_in_arc = "ip4-lookup",
		.arc_index_ptr = &ip4_main.lookup_main.ucast_feature_arc_index,
	};

在 ``ip4_unicast`` arc组中设置了两个起始node节点 ``ip4-input`` 和 ``ip4-input-no-checksum`` 。在初始化阶段，将
feature arc索引存储在结构体的参数 ``ip4_main.lookup_main.ucast_feature_arc_index`` 中。在起始头结点的函数中，执行下
述操作获取arc组索引，并按照arc中feature使能顺序来发送数据包。


.. code-block:: c

	/*获取ip4_unicast arc索引*/
	ip_lookup_main_t *lm = &im->lookup_main;
	arc = lm->ucast_feature_arc_index;
	
	/*起始头节点使用函数vnet_feature_arc_start来获取next索引，将报文传递给下一个节点*/
	vnet_feature_arc_start (arc, sw_if_index0, &next, b0);


将feature添加到feature arc
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

vpp中使用  ``宏定义`` -> ``构造函数`` -> ``声明列表`` 的模式来初始化feature arc。

.. code-block:: c

	VNET_FEATURE_INIT (ip4_not_enabled, static) =
	{
	  .arc_name = "ip4-unicast",
	  .node_name = "ip4-not-enabled",
	  .runs_before = VNET_FEATURES ("ip4-lookup"),
	};

	VNET_FEATURE_INIT (ip4_lookup, static) =
	{
	  .arc_name = "ip4-unicast",
	  .node_name = "ip4-lookup",
	  .runs_before = 0,	/* not before any other features */
	};

可以通过设置runs_before、runs_after来指定feature的先后顺序。如果runs_before=0时，表示当前arc组的最后一个节点。

启用或禁用feature功能
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

只需调用 ``vnet_feature_enable_disable`` 即可启用或禁用特定功能：

.. code-block:: c

	vnet_feature_enable_disable ("ip4-unicast", /* arc name */
                             "ip4-not-enabled",      /* feature name */
                             sw_if_index,    /* Interface sw_if_index */
                             enable_disable, /* 1 => enable 0 => disable */
                             0 /* (void *) feature_configuration */,
                             0 /* feature_configuration_nbytes */);
							 
feature_configuration 特性配置数据功能在vpp中使用比较少，主要是为了将基于接口的特定数据存储在feature 配置中，方便在
转发流程中获取到，减少一次基于接口查询的转发使用数据过程，以提供转发性能。当前版本中在 ``ipsec4-input-feature`` 节点
中有使用，感兴趣的读者可以去阅读代码。

报文分发到下一个feature节点
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

这里有两个场景：一是不带配置数据，二是带配置数据接口。其中next0就是报文要送到下一个节点在当前节点的槽位索引。

.. code-block:: c

	/*不带配置数据api接口*/
	vnet_feature_next (u32 * next0, vlib_buffer_t * b0);
        
	/*携带配置数据api接口，co是返回指向配置数据的指针*/
	co = vnet_feature_next_with_data (u32 * next0, vlib_buffer_t * b0,
			     u32 n_data_bytes);

功能分析
--------------


代码文件
~~~~~~~~~~~~~~~~

src/vnet/feature ::

		├── FEATURE.yaml
		├── feature.api
		├── feature.c
		├── feature.h
		├── feature_api.c
		└── registration.c


相关命令行
--------------

show feature [verbose]
~~~~~~~~~~~~~~~~

可以查询arc的索引及当前arc组类型所有feature的排序顺序，下面以ip4-unicast为例：

.. code-block:: shell

	show features verbose
	[16] ip4-unicast:  #ipv4单播 arc索引是16
	  [ 0]: ip4-rx-urpf-loose
	  [ 1]: ip4-rx-urpf-strict
	  [ 2]: svs-ip4
	 ....
	  [51]: ip4-vxlan-bypass
	  [52]: ip4-lookup  #ip4-lookup目前在最后，
  
这里只是ip4-unicast单播的feature挂接顺序，如果未feature使能的话，并不会挂接在ip4-input的下面。
  
show node ip4-input
~~~~~~~~~~~~~~~~
查询当前node节点下feature的信息，node类型、node的状态及索引等。

.. code-block:: shell

	vpp# show node ip4-input
	#node 类型，状态 及索引
	node ip4-input, type internal, state active, index 567
	  node function variants:
		Name             Priority  Active
		default                 0        
		icl                    -1        
		skx                    -1        
		hsw                    50    yes 

	  next nodes:#当前node已经挂接的孩子node节点情况：
		next-index  node-index               Node               Vectors
			 0          649               error-drop               0   
			 1          648               error-punt               0   
			 2          565               ip4-options              0   
			 3          581               ip4-lookup               0   
			 4          307         ip4-mfib-forward-lookup        0   
			 5          585             ip4-icmp-error             0   
			 6          562           ip4-full-reassembly          0   
			 7          569             ip4-not-enabled            0   
	  known previous nodes:#当前节点的父节点
		vmxnet3-input (8)                  rdma-input (39)                    pppoe-input (40)  

这里也可以通过show vlib graph ip4-input来查询node 图。

show interface feat
~~~~~~~~~~~~~~~~
查询指定接口下所有arc组中的使能情况：

.. code-block:: shell

	show interface feat GigabitEthernet13/0/0
	ip4-unicast:
	  ip4-sv-reassembly-feature
	  nat44-in2out

上面是接口使能nat44功能，会同时使能ip报文重组功能，用于解决分片报文nat五元组查询问题。

set interface feature
~~~~~~~~~~~~~~~~
指定接口使能某arc类下feature功能

.. code-block:: shell

	set interface feature <intfc> <feature_name> arc <arc_name> [disable]

这里需要注意一点，多次使能一个feature，会出现重复使能的情况。一般都需要代码中进行计数控制。


参考
--------

..
.. [1] https://gerrit.fd.io/r/c/vpp/+/12753/
.. [2] https://s3-docs.fd.io/vpp/22.02/developer/corearchitecture/featurearcs.html?highlight=feature
.. 
