
编译构建系统
==================

本章介绍VPP编译构建系统提供的各项功能及其实现细节。

我们首先观察、体验一下VPP构建系统提供的各项功能；然后分析VPP构建系统的整体结构，以构建系统提供的若干功能为例，分析其具体实现细节；最后将介绍如何针对Arm64平台，交叉编译VPP项目。

构建系统功能
--------------------------

VPP代码库顶层目录的Makefile文件，定义了若干构建目标。通过执行 ``make help`` 可以列举所有已定义的构建目标。

.. code-block:: bash

	$ git clone -b stable/2202 https://github.com/FDio/vpp.git && cd vpp && make help
	$ make help
	Make Targets:
	 install-dep[s]       - 安装依赖库、工具
	 wipe                 - 删除带调试信息的构建镜像及中间文件
	 wipe-release         - 删除发布版本的构建镜像及中间文件
	 build                - 构建带调试信息的镜像
	 build-release        - 构建发布版本的镜像
	 rebuild              - 删除，再重新构建带调试信息的镜像
	 rebuild-release      - 删除，再重新构建发布版本的镜像
	 run                  - 运行带调试信息的构建镜像
	 run-release          - 运行发布版本的构建镜像
	 debug                - 通过gdb运行带调试信息镜像
	 debug-release        - 通过gdb运行发布版本镜像
	 test                 - 运行单元测试
	 test-help            - 显示单元测试框架提供的帮助信息
	 pkg-deb              - 构建发布版本的DEB安装包
	 pkg-deb-debug        - 构建带调试信息的DEB安装包
	 install-ext-dep[s]   - 构建并安装依赖库镜像
	 checkstyle           - src目录编程规范检查
	 checkstyle-commit    - 补丁提交规范检查
	 checkstyle-test      - 测试代码编程规范检查
	 checkstyle-test-diff - 测试代码修改部分的编程规范检查
	 checkstyle-api       - API规范检查
	 fixstyle             - 修正编程规范
	 checkfeaturelist     - 根据各目录FEATURE.yaml生成VPP特性列表
	 docs                 - 构建基于Sphinx的项目文档
	 docs-venv            - 产生Python虚拟环境，用于编译生成VPP项目文档
	 docs-clean           - 删除产生的VPP项目文档及其中间文件

构建带调试信息的镜像
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

安装依赖库，并编译生成VPP镜像。 ``UNATTENDED=y`` 用于跳过依赖库安装过程中的确认交互步骤。

.. code-block:: bash

	$ make install-deps UNATTENDED=y
	$ make build

最终生成的带调试信息的 ``vpp`` 镜像文件位于 ``build-root/build-vpp_debug-native/vpp/bin/vpp`` 和 ``build-root/install-vpp_debug-native/vpp/bin/vpp`` 。可以通过 ``gdb`` 或 ``cgdb`` 工具调试诊断VPP镜像。

.. code-block:: bash

	$ sudo cgdb build-root/build-vpp_debug-native/vpp/bin/vpp
	(gdb) run -c build-root/build-vpp-native/vpp/startup.conf

构建发布版本的镜像
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

构建不带调试信息的发布版本的VPP镜像时，可以通过下面的命令构建。

.. code-block:: bash

	$ make install-deps UNATTENDED=y
	$ make build-release

``UNATTENDED=y`` 用于跳过依赖库安装过程中的确认交互步骤。最终生成的 ``vpp`` 镜像文件位于 ``build-root/build-vpp-native/vpp/bin/vpp`` 和 ``build-root/install-vpp-native/vpp/bin/vpp`` 。

构建发布版本的DEB安装包
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

除了直接交付发布版本的VPP二进制镜像文件，还可以制作DEB安装包，交付给最终用户。

.. code-block:: bash

	$ make install-deps UNATTENDED=y
	$ make pkg-deb

最终构建DEB安装包位于 ``build-root/*.deb`` ，可以通过 ``sudo dpkg -i build-root/*.deb`` 命令将安装包安装到系统中。

构建基于Sphinx的项目文档
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VPP项目文档基于Sphinx工具管理，文档源信息位于代码库的 ``docs`` 目录。Makefile提供若干功能用于项目文档的管理。伴随VPP各个正式版本的发布，社区也会将对应的文档发布到官网，比如vpp-22.02版本的文档https://s3-docs.fd.io/vpp/22.02/。

.. code-block:: bash

	$ make docs            - 编译生成VPP项目文档
	$ make docs-venv       - 产生Python虚拟环境，用于编译生成VPP项目文档
	$ make docs-clean      - 删除产生的VPP项目文档及其中间文件
	$ make docs-rebuild    - 重新构建VPP项目文档
	$ make make docs-spell - 检查.rst文档源信息的格式

编码格式检查
~~~~~~~~~~~~~~~~~~~~~~~~

VPP项目各个组成部分，比如C源代码、测试脚本、文档源信息、代码修改补丁格式、API格式等，都有严格的编程编码规范。

.. code-block:: bash

	$ make checkstyle           - src目录编程规范检查
	$ make checkstyle-commit    - 补丁提交规范检查
	$ make checkstyle-test      - 测试代码编程规范检查
	$ make checkstyle-test-diff - 改动测试代码的规程规范检查
	$ make checkstyle-api       - API规范检查
	$ make fixstyle             - 修正编程规范
	$ make make docs-spell      - 检查.rst文档源信息的格式
	$ make fixstyle-all         - 包含上述所有的规范检查

比如，对VPP代码做了修改之后，需要执行 ``make checkstyle`` 检查修改的代码是否符合VPP的编程规范。如果不符合要求， ``make checkstyle`` 将报错，并提示通过 ``extras/scripts/checkstyle.sh --fix`` 或 ``make fixstyle`` 命令修正不符合规范的代码改动。

构建系统结构
-----------------------------

VPP编译构建系统的整体思路是，将代码库包含的代码看作一个个的代码包。代码包有各种类型，比如：

  * src目录下，VPP项目自身的源代码
  * 依赖的开发库的代码，比如DPDK
  * 各种工具的代码，比如编译工具链

这些代码包有其各具特色的构建系统。代码包之间存在依赖关系，比如VPP自身源码依赖于DPDK提供的头文件和链接库。VPP编译构建系统实质上是一个包管理系统，管理代码包之间的依赖关系并触发代码包的构建过程。典型的包管理功能包括下载源码、校验代码完整性、加载代码修改补丁、编译构建镜像、安装镜像、制作安装包等活动。

VPP项目依赖若干开发库，比如DPDK、IPSec-MB、libbpf、quickly、rdma-core等。VPP构建系统的顶级包管理系统并没有把这些依赖的开发库视作一个个独立的代码包，而是将将他们整体是做一个代码包： ``external`` 。然后通过一个次级包管理系统管理这些依赖的开发库。该次级包管理系统同样提供所有的顶级包管理系统具备的功能。

在VPP项目发展的早期阶段，需要构建很多的依赖的代码包，甚至包括工具链。但是在现阶段，主（顶级）包系统管理所管理的代码包主要包括：

  * external.mk - 第三方的开发库被整体上是做一个代码包，进行管理，包含DPDK、IPSec-MB、libbpf、quickly、rdma-core
  * vpp.mk - VPP自身在src目录下的源码也被视作代码包，被主（顶级）包管理系统管理

该次级包管理同样提供所有的包管理系统应具备的功能。次级包系统管理所管理的依赖库，如下所示：

.. code-block:: bash

  $ ls build-data/packages/
  build.mk  external.mk  extras.mk  gmod.mk  libmemif.mk  sample-plugin.mk  src.mk  vpp.mk

VPP cmake构建系统负责src目录下自身项目的构建过程。这是一个典型的cmake/ninja构建项目。在后面小节中，将详细介绍它的细节。

从代码实现上来看，经过简化后的VPP项目构建系统结构如下表所示，主要包含三部分。

  * 主（顶级）包管理系统 - 主要包括 ``Makefile`` 、 ``build-root/Makefile`` 和 ``build-data/*`` ；
  * 次级包管理系统 - 主要包括 ``build/external/*`` 相关的文件；
  * VPP cmake构建系统 - 主要包括位于 ``src`` 目录下的各级CMakeLists.txt和 ``src/cmake/*.cmake`` 文件；

.. code-block:: bash

	├── build
	│   └── external
	│       ├── Makefile
	│       ├── packages
	│       │   ├── dpdk.mk
	│       │   ├── ipsec-mb.mk
	│       │   ├── libbpf.mk
	│       │   ├── quicly.mk
	│       │   └── rdma-core.mk
	│       ├── packages.mk
	│       ├── patches
	│       │   ├── dpdk_21.11
	│       │   ├── dpdk_22.03
	│       │   ├── ipsec-mb_1.2
	│       │   ├── quicly_0.1.3-vpp
	│       │   └── README
	│       └── rpm
	│           └── vpp-ext-deps.spec
	├── build-data
	│   ├── packages
	│   │   ├── build.mk
	│   │   ├── external.mk
	│   │   ├── extras.mk
	│   │   ├── gmod.mk
	│   │   ├── libmemif.mk
	│   │   ├── sample-plugin.mk
	│   │   ├── src.mk
	│   │   └── vpp.mk
	│   ├── platforms
	│   │   └── vpp.mk
	│   └── platforms.mk
	├── build-root
	│   ├── build-config.mk
	│   ├── copyimg
	│   ├── Makefile
	│   ├── platforms.mk
	│   ├── scripts
	│   │   ├── remove-rpath -> ../../src/scripts/remove-rpath
	│   │   ├── set-rpath
	│   │   └── version -> ../../src/scripts/version
	│   └── vagrant -> ../extras/vagrant
	├── Makefile
	├── src
	│   ├── cmake
	│   │   ├── cpu.cmake
	│   │   ├── exec.cmake
	│   │   ├── library.cmake
	│   │   ├── misc.cmake
	│   │   ├── pack.cmake
	│   │   ├── plugin.cmake
	│   ├── CMakeLists.txt

主（顶级）包管理系统
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

主（顶级）包管理系统实现代码包括： ``Makefile`` 、 ``build-root/Makefile`` 、 ``build-data/*`` 、

.. code-block:: bash

  ├── build-data
  │   ├── packages
  │   │   ├── build.mk
  │   │   ├── external.mk
  │   │   ├── extras.mk
  │   │   ├── gmod.mk
  │   │   ├── libmemif.mk
  │   │   ├── sample-plugin.mk
  │   │   ├── src.mk
  │   │   └── vpp.mk
  │   ├── platforms
  │   │   └── vpp.mk
  │   └── platforms.mk
  ├── build-root
  │   ├── build-config.mk
  │   ├── copyimg
  │   ├── Makefile
  │   ├── platforms.mk
  │   ├── scripts
  │   │   ├── remove-rpath -> ../../src/scripts/remove-rpath
  │   │   ├── set-rpath
  │   │   └── version -> ../../src/scripts/version
  │   └── vagrant -> ../extras/vagrant
  └── Makefile

VPP在顶层目录通过 ``Makefile`` 呈现构建系统提供的各种功能。

``make install-deps`` 命令汇总、安装当前Linux发布版需要的库和工具集。

.. code-block:: bash

    DEB_DEPENDS  = curl build-essential autoconf automake ccache
    DEB_DEPENDS += debhelper dkms git libtool libapr1-dev dh-python
    ...
    .PHONY: install-dep
    install-dep:
    ifeq ($(filter ubuntu debian,$(OS_ID)),$(OS_ID))
    	@sudo -E apt-get update
    	@sudo -E apt-get $(APT_ARGS) $(CONFIRM) $(FORCE) install $(DEB_DEPENDS)

``make build-release`` 功能，最终通过命令 ``make -C /home/lijzha01/tasks/vpp/build_root PLATFORM=vpp TAG=vpp vpp-install`` 调用 ``build-root/Makefile`` 文件，来构建VPP镜像。

.. code-block:: bash

    define make
    	@make -C $(BR) PLATFORM=$(PLATFORM) TAG=$(1) $(2)
    endef
    
    ...
    
    .PHONY: build-release
    build-release: $(BR)/.deps.ok
    	$(call make,$(PLATFORM),$(addsuffix -install,$(TARGETS)))

``make checkstyle`` 命令，最终调用脚本 ``extras/scripts/checkstyle.sh`` 进行编码规范检查。

.. code-block:: bash

    .PHONY: checkstyle
    checkstyle: checkfeaturelist
    	@extras/scripts/checkstyle.sh
    
``build-root/Makefile`` 包含相关makefile片段文件。

  * build-root/build-config.mk - 通过变量SOURCE_PATH定义项目源码库路径：

  .. code-block:: bash

    SOURCE_PATH = $(CURDIR)/..

  * build-root/platforms.mk - 平台相关的变量定义

  .. code-block:: bash

    PLATFORM = vpp
    native_arch = native

  * build-data/platforms.mk - 将平台相关的makefile片段包含进来，目前只有build-data/platforms/vpp.mk文件：

  .. code-block:: bash

    $(foreach d,$(SOURCE_PATH_BUILD_DATA_DIRS),     \
      $(eval -include $(d)/platforms/*.mk))

  * build-data/packages/\*.mk - 包含VPP项目依赖的开发库相关的变量定义、目标依赖和构建目标执行的动作。目前主包管理系统管理的软件包包括external.mk和vpp.mk。

前面提到过，主包管理系统分别控制build/external和src目录下第三方依赖库和VPP自身源码的构建过程。这个包管理过程将库构建过程分为主要四个步骤(实际上的步骤要远超过四个)：

  * find-source - 查找定位开发包的代码路径
  * configure - 编译构建之前，配置开发包
  * build - 编译开发包，生成镜像
  * install - 安装构建的镜像、动态链接库、静态链接库、头文件到制定目录

这四个步骤是有依赖关系的，比如install需要在build完成之后：

.. code-block:: bash

  .PHONY: %-install
  %-install: %-build
      $(install_check_timestamp)

而这四个步骤具体需要执行的动作，是定义在 ``build-data/packages/*.mk`` 中。

比如vpp包 - ``build-data/packages/vpp.mk`` 定义构建vpp包镜像时，configure、build、install步骤需要执行的各个命令： vpp_configure、vpp_build、vpp_install。

.. code-block:: bash

  vpp_configure_depend += external-install
  vpp_configure = \
    cd $(PACKAGE_BUILD_DIR) && \
    $(CMAKE) -G Ninja $(vpp_cmake_args) $(call find_source_fn,$(PACKAGE_SOURCE))
  vpp_build = $(CMAKE) --build $(PACKAGE_BUILD_DIR) -- $(MAKE_PARALLEL_FLAGS)
  vpp_install = $(CMAKE) --build $(PACKAGE_BUILD_DIR) -- install | grep -v 'Set runtime path'
  vpp-package-deb: vpp-install
          @$(CMAKE) --build $(PACKAGE_BUILD_DIR)/vpp -- pkg-deb
          @find $(PACKAGE_BUILD_DIR) \
            -maxdepth 2 \
            \( -name '*.changes' -o -name '*.deb' -o -name '*.buildinfo' \) \
            -exec mv {} $(CURDIR) \;

external包 - ``build-data/packages/external.mk`` 定义构建external包镜像时，install步骤需要执行的命令：external_install；而configure和build步骤执行的命令均为空。

.. code-block:: bash

  external_configure = echo
  external_build = echo
  external_install =  make $(DPDK_MAKE_ARGS) -C external ebuild-build ebuild-install

其中，vpp包对external包的依赖关系通过下面的变量定义实现：

.. code-block:: bash

  vpp_configure_depend += external-install

external包及次级包管理系统
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VPP依赖诸如DPDK、IPSec-MB、libbpf、quickly、rdma-core等开发库，这些开发库被主包管理系统整体上视为external包。同时，在 ``build/external`` 目录下提供了次级包管理系统，来管理DPDK、IPSec-MB、libbpf、quickly、rdma-core等开发库的构建过程。次级包管理系统，对依赖的开发库执行源码下载、打补丁、编译、安装等操作，以供编译vpp包源代码时，提供开发库头文件和库文件。

.. code-block:: bash

	└── build
	    └── external
	        ├── Makefile
	        ├── packages
	        │   ├── dpdk.mk
	        │   ├── ipsec-mb.mk
	        │   ├── libbpf.mk
	        │   ├── quicly.mk
	        │   └── rdma-core.mk
	        ├── packages.mk
	        ├── patches
	        │   ├── dpdk_21.11
	        │   ├── dpdk_22.03
	        │   ├── ipsec-mb_1.2
	        │   ├── quicly_0.1.3-vpp
	        │   └── README
	        └── rpm
	            └── vpp-ext-deps.spec

``build/external/Makefile`` 和 ``build/external/packages.mk`` 提供次级包管理系统，管理的源码库包括DPDK、IPSec-MB、libbpf、quickly、rdma-core。次级包管理系统构建源码库的主要步骤有：源码下载、源码解压、应用代码补丁、配置源码、编译、安装。每个步骤都依赖于前一步骤的完成，比如在执行安装步骤时，需要编译步骤已经结束，镜像已经构建完成。

.. code-block:: bash

  $(B)/.$1.install.ok: $(B)/.$1.build.ok
      $$(call h1,"installing $1 $($1_version) - log: $$($1_install_log)")
      $$(call $1_install_cmds)
      @touch $$@
  
  .PHONY: $1-install
  $1-install: $(B)/.$1.install.ok

``build/external/packages.mk`` 定义各个步骤目标依赖和执行命令的模板， ``build/external/packages/\*.mk`` 定义各源码库在具体步骤时，执行的命令。以 ``build/external/packages/dpdk.mk`` 为例：

.. code-block:: bash

  define dpdk_build_cmds
      cd $(dpdk_build_dir) && \
      source ../dpdk-meson-venv/bin/activate && \
      meson compile $(DPDK_VERBOSE_BUILD) -C . | tee $(dpdk_build_log) && \
      deactivate
  endef
  
  define dpdk_install_cmds
      cd $(dpdk_build_dir) && \
      source ../dpdk-meson-venv/bin/activate && \
      meson install && \
      cd $(dpdk_install_dir)/lib && \
      echo "GROUP ( $$(ls librte*.a ) )" > libdpdk.a && \
      rm -rf librte*.so librte*.so.* dpdk/*/librte*.so dpdk/*/librte*.so.* && \
      deactivate
  endef

``build/external/Makefile`` 将 ``build/external/packages.mk`` 和 ``build/external/packages/\*.mk`` 定义的makefile片段包含进来。为主包管理系统提供构建external包的入口目标。

.. code-block:: bash

  .PHONY: install
  install: $(if $(ARCH_X86_64), ipsec-mb-install) dpdk-install rdma-core-install quicly-install libbpf-install
  
  .PHONY: config
  config: $(if $(ARCH_X86_64), ipsec-mb-config) dpdk-config rdma-core-config quicly-build

  .PHONY: ebuild-build ebuild-install

  ebuild-build:
      make config

  ebuild-install:
      make install

vpp包构建系统
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VPP项目自身的源代码位于 ``src`` 目录下面，对于主包管理系统，这部分代码被视为vpp包。vpp包的构建依赖于external包，需要external包构建完毕之后，才开始vpp包的构建过程。vpp包源代码通过cmake/ninja构建镜像。功能上，cmake + ninja等效于autotools + make，两套工具都支持本地编译和交叉编译、库依赖等需求。cmake + ninja工具套件在编译效率和可移植性上更具有优势。

.. code-block:: bash

  └── src
      ├── cmake
      │   ├── api.cmake
      │   ├── ccache.cmake
      │   ├── CMakeLists.txt
      │   ├── cpu.cmake
      │   ├── exec.cmake
      │   ├── library.cmake
      │   ├── misc.cmake
      │   ├── pack.cmake
      │   ├── plugin.cmake
      │   ├── syscall.cmake
      │   └── VPPConfig.cmake
      ├── CMakeLists.txt
      ├── pkg
      │   ├── CMakeLists.txt
      │   └── debian
      │       ├── changelog.in
      │       ├── control.in
      │       ├── rules.in
      │       ├── vpp.postinst
      │       ├── vpp.postrm
      │       ├── vpp.preinst
      │       └── vpp.service
      ├── plugins
      │   ├── abf
      │   │   └── CMakeLists.txt
      ├── vnet
      │   ├── CMakeLists.txt
      ├── vpp
      │   ├── CMakeLists.txt
      │   ├── conf
      │   │   ├── 80-vpp.conf
      │   │   ├── startup.conf
      │   │   └── startup.conf.in
      ├── vpp-api
      │   ├── CMakeLists.txt
      └── vppinfra
          ├── CMakeLists.txt
          ├── config.h.in
  ... ... ...

VPP源码cmake编译配置文件结构如下所示。最顶级的VPP cmake配置文件是 ``src/CMakeLists.txt`` ，它定义VPP项目的基本信息，比如cmake工具版本、项目名称、编程语言、源码目录、编译安装目录等信息。此外 ``src/CMakeLists.txt`` 还包含两类cmake编译配置文件：

  * 规则/函数定义 - 位于 ``src/cmake/{*.cmake}`` ，定义了其他CMakeLists.txt需要用到的函数，比如链接库编译规则函数、multi-arch多文件编译函数、插件编译生成函数等等。
  * 目标定义 - 位于各子目录的CMakeLists.txt文件，通过调用 ``src/cmake/{*.cmake}`` 定义的函数，来确定需要构建的动态链接库、应用程序、插件等构建目标。

vpp包顶级编译配置文件 ``src/CMakeLists.txt`` 工作流程是：

1. 指定cmake工具的最小版本要求，至少3.13版本。设置编译器优先级，clang13 > clang12 > clang11 > clang10 > clang9 > gcc-10 > gcc-9。指定项目名称为vpp。设置链接库安装路径：CMAKE_INSTALL_LIBDIR = lib/x86_64-linux-gnu或lib/aarch64-linux-gnu。

2. 将系统和自定义cmake模块包含进来，其中包含函数实现和变量定义。这些模块的主要作用是：

.. code-block:: c

  include(CheckCCompilerFlag)  // 通过函数 ``check_c_compiler_flag(<flag> <var>)`` 查询当前编译器是否支持<flag>编译选项
  include(CheckIPOSupported)   // 通过函数 ``check_ipo_supported()`` 检查编译器是否支持中间过程优化功能
  include(GNUInstallDirs)      // 提供GNU编码标准定义的安装目录变量，比如 ``CMAKE_INSTALL_BINDIR = bin``
  include(cmake/misc.cmake)    // src/cmake/misc.cmake自定义模块实现定义message()、pr()、string_append()函数
  include(cmake/cpu.cmake)     // 实现vpp_library_set_multiarch_sources()函数，对指定源码文件，进行multi-arch编译、链接
  include(cmake/ccache.cmake)  // 启用基于ccache编译加速功能

3. 调用脚本 ``src/scripts/version`` 获取当前VPP源码版本号，比如 `VPP_LIB_VERSION = 22.02.0`。

4. 设置编译器变量 CMAKE_C_COMPILER_TARGET = x86_64 或 aarch64。

5. 设置编译器编译和链接选项：

.. code-block:: shell

  if (CMAKE_BUILD_TYPE)
    add_compile_options(-g -Werror -Wall)
  endif()
  if (${CMAKE_BUILD_TYPE_LC} MATCHES "release")
    add_compile_options(-O3 -fstack-protector -fno-common)
    add_compile_definitions(_FORTIFY_SOURCE=2)
  elseif (${CMAKE_BUILD_TYPE_LC} MATCHES "debug")
    add_compile_options(-O0 -fstack-protector -fno-common)
    add_compile_definitions(CLIB_DEBUG)
  elseif (${CMAKE_BUILD_TYPE_LC} MATCHES "coverity")
    add_compile_options(-O0)
    add_compile_definitions(__COVERITY__)
  elseif (${CMAKE_BUILD_TYPE_LC} MATCHES "gcov")
    add_compile_options(-O0 -fprofile-arcs -ftest-coverage)
    add_compile_definitions(CLIB_DEBUG CLIB_GCOV)
  endif()

6. 根据命令行设置，启动sanitizers：

.. code-block:: c

  if (VPP_ENABLE_SANITIZE_ADDR)
    add_compile_options(-fsanitize=address)
    add_compile_definitions(CLIB_SANITIZE_ADDR)
    add_link_options(-fsanitize=address)
  endif (VPP_ENABLE_SANITIZE_ADDR)

7. 设置VPP应用程序运行时链接库查找路径和VPP编译时头文件查找路径：

.. code-block:: shell

  set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${VPP_LIBRARY_DIR}")
  include_directories (
      ${CMAKE_SOURCE_DIR}
      ${VPP_BINARY_DIR}
  )

8. 包含源代码链接库、插件、应用程序编译相关的自定义cmake模块：

.. code-block:: c

  include(cmake/syscall.cmake)   // 查询编译器是否支持fcntl64()系统调用，并给编译器添加变量定义
  include(cmake/api.cmake)       // 实现函数vpp_generate_api_header()，根据*.api文件自动生成对应的头文件。添加编译目标api_headers
  include(cmake/library.cmake)   // 实现函数add_vpp_library()，添加目标链接库，并定义链接库编译规则
  include(cmake/exec.cmake)      // 实现函数add_vpp_executable()，添加目标应用程序，并定义应用程序编译规则
  include(cmake/plugin.cmake)    // 实现函数add_vpp_plugin()，添加目标插件，并定义插件编译规则

9. 遍历 ``src`` 所有子目录，添加子目录到编译系统中，cmake将逐个调用子目录下的CMakeLists.txt配置文件：

.. code-block:: shell

  set(SUBDIRS
    vppinfra svm vlib vlibmemory vlibapi vnet vpp vat vat2 vcl vpp-api
    plugins tools/vppapigen tools/g2 tools/perftool cmake pkg
    tools/appimage
  )
  foreach(DIR ${SUBDIRS})
    add_subdirectory(${DIR} ${VPP_BINARY_DIR}/${DIR})
  endforeach()

10. 添加若干PHONY目标，比如 ``run`` 、 ``debug`` ，这些目标规则将通过顶层Makefile呈现给用户。

11. 输出cmake配置信息，比如VPP_VERSION、CMAKE_C_COMPILER、CMAKE_PREFIX_PATH、CMAKE_INSTALL_PREFIX等等。

**vppinfra子目录**

``src/vppinfra`` 目录提供VPP项目基本的数据类型和结构。 ``src/vppinfra/CMakeLists.txt`` 添加链接库目标，生成libvppinfra.so动态链接库。变量VPPINFRA_SRCS定义生成动态链接库的源代码文件，变量VPPINFRA_HEADERS定义伴随链接库需要一起安装的头文件，变量LINK_LIBRARIES定义动态链接库依赖的其他链接库。

.. code-block:: cmake

    add_vpp_library(vppinfra
      SOURCES ${VPPINFRA_SRCS}
      LINK_LIBRARIES m ${EXECINFO_LIB}
      INSTALL_HEADERS ${VPPINFRA_HEADERS}
      COMPONENT libvppinfra
      LTO
    )

``add_vpp_library()`` 函数的实现细节参考文件 ``src/cmake/library.cmake`` 。

**plugins子目录**

``src/plugins`` 目录的每个子目录包含一个插件的源代码。``src/plugins/CMakeLists.txt`` 遍历其下面的所有子目录，将各个插件子目录添加到整个构建系统。

.. code-block:: cmake

	##############################################################################
	# find and add all plugin subdirs
	##############################################################################
	FILE(GLOB files RELATIVE
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/*/CMakeLists.txt
	)
	foreach (f ${files})
	get_filename_component(dir ${f} DIRECTORY)
	add_subdirectory(${dir})
	endforeach()

例如， ``abf`` 插件目录 ``src/plugins/abf`` 被添加到构建系统中，编译过程将运行该子目录的编译配置文件 ``src/plugins/abf/CMakeLists.txt`` 。 ``src/plugins/abf/CMakeLists.txt`` 调用 ``src/cmake/plugin.cmake`` 定义的函数add_vpp_plugin()，生成abf-plugin插件的编译构建规则。

.. code-block:: cmake

	add_vpp_plugin(abf
	SOURCES
	abf_api.c
	abf_itf_attach.c
	abf_policy.c
	
	API_FILES
	abf.api
	)

**src/vpp子目录**

这个目录下的CMakelists.txt文件主要添加应用程序相关的目标，及其构建细节。通过这个文件构建的应用程序包括：vpp、vppctl等。以vpp应用程序的构建配置为例，VPP_SOURCES变量定义vpp程序的源码文件，LINK_LIBRARIES变量指向vpp程序依赖的链接库名称。构建出来的vpp程序将会安装到指定目录里。

.. code-block:: cmake

    add_vpp_executable(vpp
      ENABLE_EXPORTS
      SOURCES ${VPP_SOURCES}
      LINK_LIBRARIES svm vlib vppinfra vlibmemory vnet Threads::Threads ${CMAKE_DL_LIBS}
      DEPENDS vpp_version_h api_headers
    )

``add_vpp_executable()`` 函数的实现细节参考文件 ``src/cmake/exec.cmake`` 。

VPP构建命令详解
--------------------

本小节分析一下具体VPP构建命令的细节。

make install-dep
~~~~~~~~~~~~~~~~

命令 ``make install-dep`` 和 ``make install-deps`` 等效。该命令收集当前Linux发布版本需要安装的库、工具集。VPP-22.02支持的Linux发布版本包括Ubuntu-20.04、Ubuntu-20.10、Debian-10、Fedora、CentOS-8等。若用户使用其他的Linux发布版本，那么需要自行安装依赖的库、工具集。

.. code-block:: makefile

  .PHONY: install-dep
  install-dep:
  ifeq ($(filter ubuntu debian,$(OS_ID)),$(OS_ID))
      @sudo -E apt-get update
      @sudo -E apt-get $(APT_ARGS) $(CONFIRM) $(FORCE) install $(DEB_DEPENDS)

make build-release
~~~~~~~~~~~~~~~~~~

``make build-release`` 构建VPP版本版镜像。该命令的目标依赖 ``make install-dep`` 执行生成的时间戳文件，所以之前没有运行 ``make install-dep`` 命令，直接执行 ``make build-release`` 命令，将隐式地首先执行 ``make install-dep`` 命令。

.. code-block:: makefile

  .PHONY: build-release
  build-release: $(BR)/.deps.ok
      $(call make,$(PLATFORM),$(addsuffix -install,$(TARGETS)))

它实际调用 ``build-root/Makefile`` 文件：

.. code-block:: shell

  make -C $(BR) PLATFORM=vpp TAG=vpp vpp-install

也即：

.. code-block:: shell

  make -C /home/lijzha01/vpp/build-root PLATFORM=vpp TAG=vpp vpp-install

``build-root/Makefile`` 主包构建管理系统，定义目标 ``vpp-install`` 构建规则如下。 ``vpp-install`` 目标的依赖于 ``vpp-build`` 。这也符合包构建管理系统常规的构建包镜像的一般步骤：下载源码、配置、编译、安装。

.. code-block:: makefile

  install_check_timestamp =                     \
    inst=$(TIMESTAMP_DIR)/$(INSTALL_TIMESTAMP) ;\
    $(install_package) ;                        \
    touch $${inst} ;

  .PHONY: %-install
  %-install: %-build
      $(install_check_timestamp)

通过变量install_package，实际调用 ``build-data/packages/vpp.mk`` 文件的vpp-install定义的执行命令。

.. code-block:: makefile

  vpp_configure = \
    cd $(PACKAGE_BUILD_DIR) && \
    $(CMAKE) -G Ninja $(vpp_cmake_args) $(call find_source_fn,$(PACKAGE_SOURCE))
  vpp_build = $(CMAKE) --build $(PACKAGE_BUILD_DIR) -- $(MAKE_PARALLEL_FLAGS)
  vpp_install = $(CMAKE) --build $(PACKAGE_BUILD_DIR) -- install | grep -v 'Set runtime path'

make wipe-release
~~~~~~~~~~~~~~~~~

``make wipe-release`` 命令删除构建镜像及其中间过程文件。

.. code-block:: makefile

  .PHONY: wipe-release
  wipe-release: test-wipe $(BR)/.deps.ok
      $(call make,$(PLATFORM),$(addsuffix -wipe,$(TARGETS)))

它实际调用 ``build-root/Makefile`` 文件：

.. code-block:: shell

  make -C $(BR) PLATFORM=vpp TAG=vpp vpp-wipe

也即：

.. code-block:: shell

  make -C /home/lijzha01/vpp/build-root PLATFORM=vpp TAG=vpp vpp-wipe

``build-root/Makefile`` 主包构建管理系统，定义目标 ``vpp-wipe`` 构建规则如下。它将删除编译目录 ``/home/lijzha01/vpp/build-root/build-vpp-native`` 和安装目录 ``/home/lijzha01/vpp/build-root/install-vpp-native`` 下所有的文件。

.. code-block:: makefile

  package_wipe_script = \
    rm -rf $(if $(is_build_tool),$(PACKAGE_BUILD_DIR),$(PACKAGE_INSTALL_DIR) $(PACKAGE_BUILD_DIR))
  
  .PHONY: %-wipe
  %-wipe:
      $(package_wipe_script)

make pkg-deb
~~~~~~~~~~~~

``make pkg-deb`` 将 ``make build-release`` 命令生成的镜像文件打包生成DEB安装包，交付给用户安装使用。

.. code-block:: makefile

  .PHONY: pkg-deb
  pkg-deb:
      $(call make,$(PLATFORM),vpp-package-deb)

它实际调用命令：

.. code-block:: shell

  make -C /home/lijzha01/vpp/build-root PLATFORM=vpp TAG=vpp vpp-package-deb

vpp-package-deb构建规则定义在文件 ``build-data/packages/vpp.mk`` 中：

.. code-block:: makefile

  vpp-package-deb: vpp-install
      @$(CMAKE) --build $(PACKAGE_BUILD_DIR)/vpp -- pkg-deb
      @find $(PACKAGE_BUILD_DIR) \
            -maxdepth 2 \
            \( -name '*.changes' -o -name '*.deb' -o -name '*.buildinfo' \) \
            -exec mv {} $(CURDIR) \;

上述vpp-package-deb构建规则，实际使用ninja根据cmake配置规则，执行 ``pkg-deb`` 目标的命令。而 ``pkg-deb`` 目标和构建规则，在文件 ``src/pkg/CMakeLists.txt`` 定义。通过dpkg-buildpackage工具将构建的镜像文件打包制作DEB安装包。

.. code-block:: cmake

  add_custom_target(pkg-deb
    COMMENT "Building .deb packages..."
    WORKING_DIRECTORY ${VPP_BINARY_DIR}
    COMMAND "dpkg-buildpackage" "-us" "-uc" "-b"
    USES_TERMINAL
  )

make install-ext-deps
~~~~~~~~~~~~~~~~~~~~~

``make install-ext-deps`` 编译构建DPDK、IPSec-MB、rdma-core等开发库，制作这些开发库的DEB安装包，并将其安装到系统目录中。这样后续在编译构建VPP镜像时，将不需要重复编译构建这些依赖的开发库，提高项目编译构建效率。

.. code-block:: makefile

  .PHONY: install-ext-deps
  install-ext-deps:
      make -C build/external install-$(PKG)

实际执行 ``/home/lijzha01/tasks/vpp/build/external/Makefile`` 目标 ``install-deb`` 的构建命令。它也是使用dpkg-buildpackage工具打包DPDK、IPSec-MB等开发库的镜像，制作DEB安装包。

.. code-block:: makefile

  $(DEV_DEB): deb/debian/changelog
      @cd deb && dpkg-buildpackage -b -uc -us
      git clean -fdx deb

  install-deb:
  ifneq ($(INSTALLED_VER),$(DEB_VER)-$(PKG_SUFFIX))
      @make $(DEV_DEB)
      @sudo dpkg -i $(DEV_DEB)

交叉编译
------------------------

这里探讨的是在x86_64或aarch64架构，且安装Ubuntu-20.04发布版的服务器平台上针对其他aarch64 CPU的交叉编译过程。对于其他类型的构建平台和目标平台，其过程类似。

对于VPP这种中等代码规模、依赖于多个开发库的项目来说，支持交叉编译是比较繁琐的一件事情。VPP依赖于多个开发库，除了 ``build/external/Makefile`` 文件显式声明依赖的DPDK、IPSec-MB、libbpf、quickly、rdma-core等开发库，还有在VPP cmake构建配置文件CMakeLists.txt中声明依赖的库，包括openssl、numa、libnl等等。这意味交叉编译除了要编译VPP自身的源代码，还要交叉编译各种依赖的开发库。这显然会大大增加交叉编译VPP的工作量。

VPP通过插件机制扩展功能，有些插件依赖特定的开发库。VPP对于各种依赖库会在构建配置文件中进行是否存在的判断，如果缺乏特定的开发库，那么对应的插件将不再编译和支持。所以进行交叉编译时，可以判断是否需要特定功能的插件。如果不需要的话，那么就不需要交叉编译该插件所依赖的开发库。

比如，如果不需要DPDK之外的其他PMD驱动，那么可以在 ``build/external/Makefile`` 文件设置只依赖DPDK开发库。

.. code-block:: diff

  diff --git a/build/external/Makefile b/build/external/Makefile
  index 55443b0c6..589a45298 100644
  --- a/build/external/Makefile
  +++ b/build/external/Makefile
  @@ -48,10 +48,10 @@ clean:
      @rm -rf $(B) $(I)
  
   .PHONY: install
  -install: $(if $(ARCH_X86_64), nasm-install ipsec-mb-install) dpdk-install rdma-core-install quicly-install libbpf-install
  +install: dpdk-install
  
   .PHONY: config
  -config: $(if $(ARCH_X86_64), nasm-config ipsec-mb-config) dpdk-config rdma-core-config quicly-build
  +config: dpdk-config

安装交叉编译工具链
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

首先在Arm官网下载针对适用特定构建平台和目标平台的的交叉编译工具链 [1]_，安装到指定的目录：

.. code-block:: shell

  $ wget https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu.tar.xz
  $ tar -xvf gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu.tar.xz
  $ cd gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu
  $ find . -type f -exec sudo install -D {} ${INSTALL_DIR}/{} \;
  $ find . -type l -exec sudo cp -d {} ${INSTALL_DIR}/{} \;

将交叉工具链添加到PATH环境变量：

.. code-block:: shell

  export PATH=${PATH}:${INSTALL_DIR}/bin:${INSTALL_DIR}/usr/bin:${INSTALL_DIR}/usr/local/bin

验证交叉工具链是否正常运行：

.. code-block:: shell

  aarch64-none-linux-gnu-gcc --version

交叉编译DPDK源码
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

DPDK采用meson+ninja的构建系统，其交叉编译过程参考DPDK文档中关于交叉编译步骤的描述 [2]_。根据交叉工具链类型和安装路径，修改对应的meson配置文件。

.. code-block:: bash

  cat ./config/arm/arm64_armv8_linux_gcc
  [binaries]
  c = 'aarch64-none-linux-gnu-gcc'
  cpp = 'aarch64-none-linux-gnu-cpp'
  ar = 'aarch64-none-linux-gnu-gcc-ar'
  strip = 'aarch64-none-linux-gnu-strip'
  pkgconfig = 'aarch64-none-linux-gnu-pkg-config'
  pcap-config = ''
  
  [host_machine]
  system = 'linux'
  cpu_family = 'aarch64'
  cpu = 'armv8-a'
  endian = 'little'
  
  [properties]
  # Generate binaries that are portable across all Armv8 machines
  platform = 'generic'
  c_args = ['-I${INSTALL_DIR}/include', '-I${INSTALL_DIR}/usr/local/include']
  c_link_args = ['-L${INSTALL_DIR}/usr/local/lib', '-L${INSTALL_DIR}/usr/local/lib/aarch64-linux-gnu', '-L${INSTALL_DIR}/usr/lib/aarch64-linux-gnu', '-L${INSTALL_DIR}/lib/aarch64-linux-gnu']

然后在编译DPDK源码时，指定该交叉编译配置文件即可：

.. code-block:: shell

  $ meson build --cross-file config/arm/arm64_armv8_linux_gcc
  $ cd build
  $ ninja

交叉编译VPP源码
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VPP自身源码位于 ``src`` 目录，采用cmake+ninja构建工具。cmake进行交叉编译时，首先添加交叉编译配置文件：

.. code-block:: shell

  $ cat src/cmake/aarch64_toolchain.cmake
  set(CMAKE_SYSTEM_NAME "Linux")
  set(CMAKE_SYSTEM_PROCESSOR "aarch64")
  set(CMAKE_C_COMPILER "aarch64-none-linux-gnu-gcc")
  set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
  set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
  set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)

在执行命令 ``make build-release`` 时，通过命令行参数，添加交叉工具链到环境变量，指定交叉编译配置文件，设置依赖的头文件和链接库的地址。

.. code-block:: shell

  $ PATH=${PATH}:${INSTALL_DIR}/bin \
    EXTRA_INSTALL_PATH=${INSTALL_DIR} \
    VPP_EXTRA_CMAKE_ARGS=" \
      -DCMAKE_LIBRARY_PATH=${INSTALL_DIR}/lib \
      -DCMAKE_TOOLCHAIN_FILE=${SRC_DIR}/src/cmake/aarch64_toolchain.cmake \
      -DVPP_EXTRA_INSTALL_PATH=${INSTALL_DIR}" \
    make build-release

交叉编译VPP和DPDK的完整步骤，请参考随书代码。

参考
----

.. [1] https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/downloads
.. [2] https://doc.dpdk.org/guides/linux_gsg/cross_build_dpdk_for_arm64.html