
VPP软件架构
==================

本篇介绍VPP软件架构及软件实现细节相关的知识。

在本书第一篇介绍过，VPP数据面实际上由一系列转发节点构成的报文处理流程图组成。一旦输入节点（比如 ``dpdk-input`` 节点）收到报文数组，报文处理流程图启动，向量化的高效地处理、转发报文。这种转发模型可以充分利用CPU架构的特性，优化代码，提高报文处理性能。在报文处理流程图中，除了输入节点和输出节点和硬件有联系之外，流程图的其他部分完全纯软件实现，具有很好的可移植性。

在多核CPU平台上，通常为每个工作负载CPU启动一个专门的线程；并通过CPU亲和性设置，将该线程固定在该CPU上运行。并且为每个工作线程复制一份独立的报文处理流程图，减少CPU间数据依赖，保证各个CPU独立高效的运行。特定网卡的特定队列上收到的报文，固定的送给特定的工作线程处理。网卡的负载均衡机制确保报文流程能够均衡的送给各个工作状态的线程。

VPP软件框架包含基础设施层VPP INFRA、矢量处理库VLIB、网络层VNET、插件集Plugins。如下图所示，越往核心，代码处于越底层位置。

.. figure:: ../images/software-architecture/VPP_Layering.png
   :align: center

   VPP软件架构图

* VPP Infra - VPP基础设施层

    * 源码路径： src/vppinfra
    * VPP INFRA 包含核心库的源代码，是一系列用C语言实现的基本库的集合，比如vec、bitmap、pool、hash、fifo；还包括VPP数据面使用的高性能的数据结构，比如bihash、timer。这一层软件为诸如内存分配、控制面数据存储、数据面高速数据查找、报文处理流程图高效调度计时器等需求提供基本的数据结构定义和函数实现。

* VLIB - 矢量处理库

    * 源码路径： src/{vlib, vlibapi, vlibmemory}
    * VLIB层实现应用程序中的各种管理函数，比如数据报文缓冲管理、物理内存管理、流程图节点管理、计数统计管理、线程管理、报文跟踪、调试诊断命令行接口CLI等。

* VNET - 基本网络协议栈层

    * 源码路径： src/vnet
    * VNET是VPP实现的用户态网络协议栈，包含L2、L3、L4各层的功能。数据面，VPP支持若干网络接口类型，与外部网络和网络设备收发报文；控制面，VPP通过CLI或运行态编程API接受控制面程序的指令。

* Plugins - 插件

    * 源码路径： src/plugins
    * VPP代码库包含丰富的数据平面插件集合，以增强报文处理流程图的功能。并且允许网络开发人员，新建自定义的插件，扩展网络功能。

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   vppinfra/index
   vlib/index
   vnet/index
   plugin/index
   input-nodes/index
   build-system/index
   unittest/index
