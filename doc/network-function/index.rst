
VPP网络功能
==================

本篇详细提供了VPP各种网络功能的概要介绍、网络拓补、配置命令、节点拓扑结构及代码实现细节。

VPP项目包含下面网络相关的功能、工具链：

#. ABF - ACL Based Forwarding - 基于ACL的转发

   - 基于策略的路由
   - 转发ACL匹配命中的报文流
   - ACL表的每条规则都指向一条路径，该路径决定报文如何转发。该路径即是FIB表指示的路径，所以任何可路由转发的报文都可以通过ABF进行转发。
   - 若干ACL条目可以聚合成一个策略
   - 策略中每条ACL都有优先级
   - 若干策略可以绑定到特定接口
   - ABF作为路由路径上的入口方向的feature功能

#. ACL - Access Control List - 访问控制列表

   ACL插件根据IP地址指定访问控制策略，包括：1.为特定MAC+IP组合指定访问策略；2.为网络层和传输层指定入口和出口方向的访问控制策略；3.分片报文可以在网络层制定访问控制策略；4.有状态ACL可以针对会话制定策略

     - 入口MAC+IP ACL，针对特定MAC+IP报文进行过滤
     - 无状态入口和出口方向ACL，根据L3、L4层信息控制报文访问
     - 有状态入口和出口方向ACL，针对特定会话报文流进行访问控制

#. ADL - Allow Deny List

   实验性质的一个功能：根据源IP地址permit或deny报文；简单、高效的控制策略

#. AF_XDP驱动

   Linux kernel 5.4版本及更新版本的AF_XDP驱动

#. ARP - Address Resolution Protocol - 地址解析协议

   RFC826 ARP协议的实现
  
#. Adjacency - 邻接表

   邻接表用于管理各种链路层协议的邻居信息。此处的邻居，是指IP层面的邻居，即对于三层转发来说一跳可达，不需要经过中间设备进行三层转发。各种链路层协议通过协商或配置生成邻居信息后，将其下发给邻接表，生成邻接表表项。邻接表中记录了邻居的网络层地址（下一跳）、路由出接口、链路层协议类型、链路层地址等信息。邻接表表项的更新、删除也由各链路层协议模块通知完成。

   IPv4/IPv6转发时，设备通过查找FIB（Forwarding Information Base，转发信息库）/IPv6 FIB表项得到报文的出接口和下一跳信息，再以此出接口和下一跳为索引查找邻接表，获取到该下一跳的链路层转发信息，如链路层协议及介质类型（P2P、NBMA）、封装报文的链路层头信息等，然后根据此信息对报文进行封装后转发。

     - 一条邻接条目包含如何发送报文到相邻节点的信息；
     - 邻接表的关键信息是：出端口、链路层报文头封装信息。路由转发时，报文要按照链路层报文头封装信息修改数据链路层内容；
     - 链路层报文头封装信息和出端口类型相关：P2P类型的接口，封装信息固定；以太网类型接口，封装信息通常来自ARP协议解析；
     - 邻接表条目有完整状态和非完整状态。一旦封装信息设置完成，就处于完整状态；
     - 在VPP实现里，邻接条目位于DPO图叶结点时，说明它是终端/常规邻接条目，代表一个物理接口。若邻接条目DPO图的中间节点，说明它不是终端邻接条目，代表的不是一个物理接口，比如GRE隧道接口。中间邻接条目DPO图的中间，还需要进一步解析，以获取发送报文通过的最终物理接口；
     - ``Glean adjacencies`` 描述了如何在一个子网里广播报文；

#. BFD - Bidirectional Forwarding Detection - 双向转发检测

   基于RFC 5880标准的高速故障检测机制，两个系统建立BFD会话后，在它们之间的通道上周期性地发送BFD报文，如果一方在协商的检测时间内没有接收到BFD报文，则认为这条双向通道上发生了故障。上层协议通过BFD感知到链路故障后可以及时采取措施，进行故障恢复。
  
#. Binary API library - VPP运行态编程API

   VPP处于运行状态时，除了可以通过 ``vppctl`` 提供命令行方式控制VPP外，还提供运行态编程接口API，供其他应用程序通过API配置VPP。VPP运行态编程API支持若干编程语言，包括：C、C++、Python、Go、Java。
   
   传输透明的运行态编程API消息处理库包含如下特性： 

     - 事件日志
     - 消息执行
     - 消息处理函数注册
     - 消息答复
     - 消息跟踪
     - 消息跟踪捕获
     - 平台相关的消息处理程序调用

#. 运行态编程API共享内存和消息传输库

   运行态编程API消息传输库：

     - 消息传输链接建立和消息定义
     - 消息传输链接建立和消息处理
     - 远程调用
     - 运行态编程API调试命令行工具

#. BIER - Bit Indexed Explicit Replication - 位索引显式复制

   BIER技术采用新的思想来解决传统组播IPMC中的突出问题，那就是将组播报文要发送到目的节点的集合以Bit String的方式封装在报文头部发送。该技术允许运营商有效转发IPMC流量，而无需在中间设备中使用明确的IPMC树状态。VPP包含BIER技术的实现：

     - 基于BIER的组播(https://tools.ietf.org/html/rfc8279)
     - MPLS和非MPLS网络中BIER报文封装(https://tools.ietf.org/html/rfc8296)

#. Bonding - 端口聚合

   各种端口聚合技术的实现：

     - 主备模式
     - 链路聚合LACP模式

       - 负载均衡 L2 | L23 | L34

     - XOR均衡模式

       - 负载均衡 L2 | L23 | L34

     - 循环模式
     - 广播模式

#. 报文缓存元数据跟踪器

   跟踪报文处理流程图某一结点处理报文时，报文缓存元数据的变动情况。

#. 报文缓存监控

   监控报文处理流程图中所有节点对报文缓存池的使用情况。

#. 静态http/https服务器内建URL资源

   为静态http/https服务器添加内嵌的URL资源集合。

#. DNS域名解析器

   带缓冲的DNS域名解析器，提升域名解析性能。

#. Classify - 流分类器

   基于掩码的报文流分类、匹配器。设置灵活，可用于匹配任意用户自定义报文域。使用SIMD指令提升报文处理性能。
 
#. Cloud NAT

   云应用场景中，基于多种标准的NAT、NPAT地址转换。它允许用户实现轻量级的NAT、NPAT而无需进行实际报文的修改，无需维护地址转换会话。

     - 基于目的IP地址/端口的地址转换
     - 带条件的、基于前缀的源IP地址/端口地址转换

#. DPO - Data-Plane Objects - 数据面对象

     - DPO是一个比较通用泛化的术语，类似C++抽象基类的概念。泛指在数据面对报文执行某种操作的某些实体对象；
     - DPO实例化的具体实例，比如邻接转发、MPLS标签插入、复制；
     - DPO对象可以叠加或联结起来构成报文需要遍历的DPO处理流程图，它描绘了需要针对报文执行的动作的集合；
     - DPO处理流程图的起始点可以从VPP报文处理流程图的任意节点开始，比如L3 FIB lookup、ABF、L3XC

#. DHCP动态主机配置协议

   DHCP客户端实现，包含下列功能：

     - DHCP client (v4/v6)
     - DHCPv6 prefix delegation
     - DHCP Proxy / Option 82

#. Feature Arc功能

   基于约束的feature-arc配置；内部APIs用于将报文分发至下一配置的feature。Feature-Arc是VPP报文转发图的重要扩展机制。

     - Feature-Arc注册
     - Feature注册
     - 灵活的Feature次序约束机制
     - 高性能APIs用于将报文分发至下一配置的feaure
     - 拓扑feature分类

#. Flow基础架构

   Flow基础架构提供硬件卸载能力。
   提供四种APIs：Flow添加、删除、启用、关闭
   目前支持如下Flow类型：

     - FLOW_TYPE_IP4,
     - FLOW_TYPE_IP6,
     - FLOW_TYPE_IP4_N_TUPLE,
     - FLOW_TYPE_IP6_N_TUPLE,
     - FLOW_TYPE_IP4_N_TUPLE_TAGGED,
     - FLOW_TYPE_IP6_N_TUPLE_TAGGED,
     - FLOW_TYPE_IP4_L2TPV3OIP,
     - FLOW_TYPE_IP4_IPSEC_ESP,
     - FLOW_TYPE_IP4_IPSEC_AH,
     - FLOW_TYPE_IP4_VXLAN,
     - FLOW_TYPE_IP6_VXLAN,
     - FLOW_TYPE_IP4_GTPC,
     - FLOW_TYPE_IP4_GTPU,
     - FLOW_TYPE_GENERIC

   作用在特定Flow类型上的动作，包含：

     - FLOW_ACTION_COUNT,
     - FLOW_ACTION_MARK,
     - FLOW_ACTION_BUFFER_ADVANCE,
     - FLOW_ACTION_REDIRECT_TO_NODE,
     - FLOW_ACTION_REDIRECT_TO_QUEUE,
     - FLOW_ACTION_RSS,
     - FLOW_ACTION_DROP

#. G2图形事件日志查看器

   高度可扩展的图形事件日志查看器，专门定制用于呈现src/vppinfra/elog.[ch]日志文件。

     - Vppinfra elog.[ch] log viewer
     - Scales to over 10e6 events, 10e4 tracks
     - Automated anomaly detector
     - View snapshots

#. GPRS隧道协议

   GPRS隧道协议的实现，包含如下功能：

     - GTPU decapsulation
     - GTPU encapsulation
 
#. GRE

   Generic Routing Encapsulation (GRE)协议实现

     - L3 tunnels, all combinations of IPv4 and IPv6
     - Encap/Decap flags to control the copying of DSCP, ECN, DF from overlay to underlay and vice-versa.
     - L2 tunnels

#. HSI (Host Stack Intercept)主机栈拦截技术

   该功能选择性的将特定报文流拦截、发送到主机栈处理。

#. Hash基础架构

   Hash基础架构

     - Ethernet
     - IP

#. IP邻居数据库

   - IP层协议无关的邻居数据库，也即peers
   - peers数量限制、重复回收、老化机制
  
#. IPSec

   IPSec协议的实现

     - IPSec (https://tools.ietf.org/html/rfc4301)
     - Authentication Header (https://tools.ietf.org/html/rfc4302)
     - Encapsulating Security Payload (https://tools.ietf.org/html/rfc4303)

#. IP in IP隧道协议 

   基于RFC2473 IP{v4,v6} over IP{v4,v6} 隧道协议的实现；也包括6RD边界中继协议(RFC5969)的实现。
   
     - IPv4/IPv6 over IPv4/IPv6 encapsulation

       - Fragmentation and Reassembly
       - Configurable MTU
       - Inner to outer Traffic Class / TOS copy
       - Configurable Traffic Class / TOS
     
     - ICMPv4 / ICMPv6 proxying
     - 6RD (RFC5969)

       - Border Relay

   还未实现的功能包括：

     - Tunnel PMTUD
     - Tracking of FIB state for tunnel state
     - IPv6 extension headers (Tunnel encapsulation limit option)

#. IPFIX测试

   IPFIX 报文流探测，工作于L2、IP输入、输出路径.
   
     - L2 input and output feature path
     - IPv4 / IPv6 input and output feature path
     - Recording of L2, L3, and L4 information 
   
   还未实现的功能包括：

     - Export over IPv6
     - Export over TCP/SCTP

#. 基于Intel IPSecMB的IPSec加密引擎

   支持如下加解密算法：

     - SHA(1, 224, 256, 384, 512)
     - CBC(128, 192, 256)
     - GCM(128, 192, 256)

#. 基于openssl的IPSec加密引擎

   支持如下加解密算法：

     - SHA(1, 224, 256, 384, 512)
     - CBC(128, 192, 256)
     - GCM(128, 192, 256)
     - CTR(128, 192, 256)
     - DES, 3DES
     - MD5

#. 基于原生实现的IPSec加密引擎

   VPP原生的加解密引擎：

     - CBC(128, 192, 256)
     - GCM(128, 192, 256)

#. IPv6邻居发现和代理 

   遵循RFC4861和RFC4862标准的IPv6邻居发现协议实现。
   遵循RFC4389标准的给定接口邻居代理协议实现。

     - Neighbor discovery.
     - ND Auto address configuration
     - Multicast Listener Discovery - only as host role to send adverts
     - Router Advertisements
     - ND (mirror) proxy on given interface

#. IGMP实现

   IGMP协议实现，只支持IGMPv3协议。

#. L2TPv3功能

   遵循RFC3931协议的L2TPv6功能。目前处于草案未完成状态，且已不再维护。

     - L2TPv3 over IPv6

#. 二层转发功能

   - 二层一对端口一对一交叉直连功能
   - 多端口在同一桥接域中桥接转发功能

     - 基于报文目的MAC地址交换转发功能
     - 基于桥接域或端口使能/禁止MAC地址学习功能
     - 使能/禁止MAC地址老化功能
     - 端口关闭、桥接域删除或用户主动设置情形下，清空已学习MAC地址
     - 用户添加静态MAC地址，不被地址老化删除或自动MAC学习覆盖
     - 用户添加MAC地址，不被地址老化影响，但会被MAC学习覆盖
     - 使能/禁止单播报文转发
     - 使能/禁止未知单播报文泛洪
     - 使能/禁止组播或广播泛洪
     - ARP终结功能，防止ARP Request泛洪
     - 使能/禁止ARP Request单薄而非泛洪
     - BVI (Bridge Virtual Interface)用于转发来自或去往特定桥接域的IP报文
     - 设置特定桥接域某一端口发送未知单播报文，而非泛洪
     - 桥接域端口SHG功能

   - 二层桥接或直连端口VLAN标签修改功能
 
#. 三层交叉直连功能

   - 交叉直连某三层端口，将其所有入口流量发送至特定FIB路径
   - 该FIB路径可以表示任意出端口
   - 可以通过VRF实现相同的功能：配置专门VRF，添加默认路由条目，默认路由条目指向该FIB路径
   - L3XC性能和效率更高

#. 链路聚合控制协议 - LACP  

   LACP链路聚合控制协议的实现。

#. 数据链路发现协议 - LLDP

   LLDP数据链路发现协议的实现。

#. Linux控制面

   该系列插件提供和Linux网络协议栈结合的机制。

   ``linux_cp`` 插件镜像VPP端口到Linux内核。意味着，对于任意VPP端口，可以在Linux内核创建对应的TAP/TUN设备，在VPP中将VPP端口和该TAP/TUN设备耦合起来。
   耦合机制在收发方向的操作不同：

     - 在接收方向，VPP端口收到的发往控制面的报文，都将被发送到耦合的TAP/TUN设备，包括ARP, ND等报文；所以关闭VPP本身的ARP, ND, ping插件。
     - 在发送方向，VPP通过耦合的TAP/TUN设备收到的报文，都被一一直连方式放到耦合的VPP端口。对于普通IP报文，VPP出端口的各种功能特性都会生效。

   ``linux_nl`` 插件监听内核netlink信息，并将IP配置信息同步到耦合的VPP接口。

#. 负载均衡

   支持如下各种模式：

     - GRE tunnel mode
     - NAT mode
     - L3DSR mode
     - Consistent Hash
     - Connection Track

#. LIST协议控制面

   LIST协议控制面实现，包含如下特性：

     - ITR, ETR and RTR mode of operation
     - Multitenancy
     - Multihoming
     - Source/dest map-cache lookups
     - RLOC-probing
     - Support for Ethernet, IPv4, IPv6 and NSH EIDs (payloads)
     - Map-resolver failover algorithm

#. LIST-GPE协议

   LIST-GPE协议实现，包含如下功能：

     - ITR, ETR and RTR modes
     - Support for Ethernet, IPv4, IPv6 and NSH EIDs (payloads)
     - Source/dest forwarding
     - IPv4 and IPv6 RLOCs
 
#. 地址/端口映射

   Mapping of Address and Port (MAP): IPv4 as a service mechanisms. Tunnel or translate an IPv4 address, an IPv4 subnet or a shared IPv4 address. In shared IPv4 address mode, only UDP, TCP and restricted ICMP is supported.

     - LW46 BR (RFC7596)
       - Fragmentation and Reassembly
     
     - MAP-E BR (RFC7597)
     - MAP-T BR (RFC7599)

#. MPLS协议

   MPLS协议实现：

     - Label imposition/disposition - pipe and uniform mode
     - Tunnels - unidirectional

#. NSH

   NSH for SFC
     
     - NSH Classifier
     - NSH Forwarder
     - NSH SF
     - NSH Proxy
     - NSH OAM
     - NSH Metadata

#. Netmap设备

   创建用户态、高性能netmap接口，通过该接口VPP可以链接特定Linux名字空间、Linux容器、物理接口。

#. NAT - 网络地址转换

   NAT插件提供多种地址转换功能。

     - NAT44-EI

       - IPv4 Endpoint Independent NAT
       - 1:1 NAT
       - 1:1 NAT with ports
       - VRF awareness
       - Multiple inside interfaces
       - Hairpinning
       - IPFIX
       - Syslog
       - TCP MSS clamping
       - Local bypass (DHCP)

     - NAT44-ED

       - IPv4 Endpoint Dependent NAT
       - 1:1 NAT
       - 1:1 NAT with ports
       - VRF awareness
       - Multiple inside interfaces
       - Hairpinning
       - IPFIX
       - Syslog
       - TCP MSS clamping
       - Local bypass (DHCP)

     - DET44 - deterministic NAT (CGN)
     - NAT64
     - NAT66
     - DSLITE
     - 464XLAT

#. 网络延迟模拟器

   可配置的网络延迟、网络丢失模拟器

#. 报文生成器

   高性能报文生成器：

     - 高性能报文生成
     - 报文定义命令
     - 支持pcap报文捕获、重发
     - 多线程报文生成
     - 支持报文注入任意报文转发图节点
     - 单元测试普遍使用

#. PPPoE协议  

   基于以太网的PPPoE协议实现：
   
     - PPPoE Control Plane packet dispatch
     - PPPoE decapsulation
     - PPPoE encapsulation

#. Pipe设备

   创建管道设备接口，可以双向收发报文，将报文发送至管道的另一端。虽然很像，但实际上并不是Linux管道。

     - L4 checksum offload

   还未支持功能:  
     - does not use hw-address
     - does not support tagged traffic
     - API dump filtering by sw_if_index
 
#. 基于策略的1：1 NAT

   基于规则匹配报文，并根据指令转换IP地址；规则保存在基于bihash的流缓存表中；指令保存在基于pool数据结构的转换表中。
   
   对于特定的端口+方向组合，其所有规则使用相同的查找掩码，即SA + SP。
   
   若流缓冲表查找未命中，报文将被转至慢速路径。查找未命中默认行为可配置，默认配置未原封不动转发报文。

#. QUIC协议

   IETF QUIC协议的实现：

     - 可通过会话层结合主机协议栈
     - 基于Quicly代码库: https://github.com/h2o/quicly

#. QoS功能 

   QoS功能的实现：

     - 记录 - 从报文头解析QoS位域，记录至元数据
     - 映射 - 定义报文QoS位域的简单转换
     - 标记 - 将映射之后的QoS位域写入报文头
     - 存储 - 报文元数据设置固定的QoS值

#. SRTP - 安全实时传输协议 

   基于libsrtp2 SRTP传输层协议的实现。

#. SRv6 - Service Chaining Dynamic Proxy 

   SRv6 dynamic proxy

#. SRv6 - Service Chaining Flow-based Dynamic Proxy

   SRv6 flow-based dynamic proxy

#. SRv6 - Service Chaining Masquerading Proxy

   SRv6 masquerading proxy

#. SRv6 - Service Chaining Static Proxy

   SRv6 static proxy

#. SRv6 Mobile

   SRv6 Mobile End Functions. GTP4.D, GTP4.E, GTP6.D, GTP6.D.Di and GTP6.E are supported.

     - GTP4.D
     - GTP4.E
     - GTP6.D
     - GTP6.D.Di
     - GTP6.E
 
#. Segment Routing for IPv6 (SRv6)

   Full SRv6 Network Programming implementation

     - Support for SRv6 Network Programming (draft-ietf-spring-srv6-network-programming-07)
     - SR Headend behaviors (H.Encaps, H.Encaps.Red, H.Encaps.L2, H.Encaps.L2.Red)
     - SR Endpoint behaviors (End, End.X, End.T) for intermediate TE with PSP support
     - SR Endpoint behaviors (End.DX4, End.DX6, End.DT4, End.DT6, End.DX2) for overlay creation
     - SR Endpoint behaviors (End.B6.Encaps.Red) for BindingSID instantiation
     - Support for SRH insertion (draft-filsfils-spring-srv6-net-pgm-insertion-01)
     - SR counters
     - SR policy implementation (draft-ietf-spring-segment-routing-policy-02)
     - SR steering based on IP prefix / L2 interface classification

#. Segment Routing for MPLS

   SR-MPLS

     - SR Policy support
     - Automated steering (SR steering based on NextHop/Color)

#. 会话层

   会话层用于北向应用程序和南向传输层协议之间的通信。对于北向，通过应用层接口子层，会话层向应用程序暴露APIs和会话层进行通信。对于南向，会话层暴露APIs允许传输层协议和应用程序交换数据和事件，而无需了解通信的详细细节。

     - 管理分配、跟踪会话 - 通过6元组查找表
     - 通过名字空间限制应用程序可访问的资源
     - 在传输层和应用程序之间转数据和控制信息
     - 传输层协议接口
       - 提供通用传输层协议模板
       - 在传输层和应用程序之间转换数据
       - 在发送方向调度会话/连接

     - 应用程序接口
       - 维护应用程序状态
       - 管理共享内存资源的分配 - 应用程序和传输层通过共享内存资源交换数据
       - 提供原生C和二进制APIs提供内嵌或外部应用程序使用

#. 源VRF选择

   - 基于源IP地址选择入口VRF表
   - 添加路由条目到VRF表
   - 基于报文源IP地址的路由查找
   - 在路由表中设置路由条目，用于在接下来的路由查找时进行查表
   - 表绑定在端口
   - 在L3转发路径中，SVS作为入口方向特性运行

#. 静态http/https服务 

   静态http/https服务作为VPP协议栈的内置应用程序：

   - HTTP GET/POST handling
   - LRU file caching
   - pluggable URL handlers
   - builtin json URL handles

     - version.json - vpp version info
     - interface_list.json - list of interfaces
     - interface_stats - single interface via HTTP POST
     - interface_stats - all intfcs via HTTP GET."

#. TLS OpenSSL功能

   VPP协议栈的TLS OpenSSL插件

     - OpenSSL engine for TLS
     - TLS Async framework
     - Enable QAT for crypto offload

#. Tap设备

   创建TAPv2设备接口，用于连接Linux主机栈的TAP设备：

     - Virtio
     - Persistence
     - Attach to an existing tap at host
     - Filter packet dump output with SW if index

#. 基于时间间隙的MAC地址过滤

   设备输入/输出处，驱动级别的MAC地址过滤。用于查看是否允许给定MAC地址的报文流。一般用于家庭网关场景，按流量收费。

#. TCP协议

   高性能、可扩展的TCP协议实现：

     - 核心功能 - RFC793, RFC5681, RFC6691
     - 高性能扩展 - RFC7323
     - 拥塞控制功能 - RFC3465, RFC8312     
     - 链接丢失重建扩展 - RFC2018, RFC3042, RFC6582, RFC6675, RFC6937
     - Detection and prevention of spurious retransmits (RFC3522)
     - Defending spoofing and flooding attacks (RFC6528)
     - Partly implemented features (RFC1122, RFC4898, RFC5961)
     - Delivery rate estimation (draft-cheng-iccrg-delivery-rate-estimation)

#. TLS - Transport Layer Security

   TLS协议的实现，包含一系列引擎，其封装了的既有TLS实现，比如：OpenSSL, Picotls and MbedTLS；核将这些引擎融入VPP主机栈的框架。 and a framework that integrates the engines into VPP's host stack

     - 支持插件式TLS引擎的框架
     - OpenSSL, Picotls and MbedTLS引擎

#. 隧道架构

   实现隧道技术的架构，支持常见类型的IP隧道功能。

#. UDP - 用户数据报协议

   用户数据报协议UDP的实现：

     - 借助会话层融入主机栈
     - 独立的每个端口调度程序用于隧道协议
 
#. GSO技术

   通用分段卸载技术：
   
     - Basic GSO support
     - GSO for VLAN tagged packets
     - GSO for VXLAN tunnel
     - GSO for IP-IP tunnel
     - GSO for IPSec tunnel
     - Provide inline function to get header offsets
     - Basic GRO support
     - Implements flow table support

#. VCL

   VCL通过暴露并提供与POSIX类似但并不兼容的API接口，来简化应用程序和会话层的交互过程。
   
     - Abstracts vpp host stack sessions to integer session handles
     - Exposes its own async communication functions, i.e., epoll, select, poll
     - Supports multi-worker applications
     - Sessions cannot be shared between multiple threads/processes
     - VCL Locked Sessions (VLS)
       - Ensure through locking that only one thread accesses a session at a time
       - Detects and registers forked processes as new VCL workers. It does not register threads as new workers.
     
     - LD_PRELOAD shim (LDP)
       - Intercepts syscalls and injects them into VLS.
       - Applications that are supported work with VCL and implicitly with VPP's host stack without any code change
       - It does not support all syscalls and syscall options

#. VPP基础架构库

   VPP基础架构支持库，包括VPP代码依赖的基础数据结构和算法。

     - Abstract device driver ring support
     - Address sanitizer support
     - Altivec, Neon, MMX, AVX2, AVX512 SIMD vector unit support
     - Atomic op support
     - Backtrace support
     - Bitmaps
     - Bounded-index extensible hashing templates
     - C11 safe-string support
     - Cache control primitives, including prefetching
     - C-dynamic arrays (vectors)
     - Circular doubly-linked list support with a head sentinel
     - Contiguous N x fixed block allocator
     - CPU clock based timebase support
     - Doubly-linked list support
     - ELF file parser
     - Endian-order support
     - Error return / reporting support
     - FIFO support
     - Fundamental types, u8, u16, u32, and so on
     - High-performance event logger
     - High-performance memcpy support
     - High-performance mmap-based circular log support
     - High-performance timer-wheel templates
     - Linux socket support
     - Linux sysfs file parsing support
     - Low-level CPU support
     - Mapped pcap file support
     - Memory allocator, "Doug Lea" malloc with a few tweaks
     - Minimal overhead Linux system-call support
     - Multi-architecture setjmp / longjmp support
     - Numerous Unit tests
     - Physical memory allocator support
     - Pools, a high performance fixed block allocation scheme
     - Red/black trees.
     - Rigorously vetted linear congruential random numbers (32 and 64 bit)
     - Serialization / unserialization support
     - SHA256, SHA512 support
     - Simple first-fit virtual space allocator
     - Simple hashing support
     - Simple macro expander
     - Sparse vector support
     - Spinlock support
     - Time Range support
     - Unix / Linux errno support
     - Vector-based printf / scanf equivalents (format, unformat)
     - Warshall's algorithm (positive transitive closure of a relation)
     - XXhash support

#. Virtio

   Virtio实现：

     - Driver mode to emulate PCI interface presented to VPP from the host interface.
     - Device mode to emulate vhost-user interface presented to VPP from the guest VM.
     - Support virtio 1.0 in virtio
     - Support virtio 1.1 packed ring in virtio [experimental]
     - Support multi-queue, GSO, checksum offload, indirect descriptor, jumbo frame, and packed ring.
     - Support virtio 1.1 packed ring in vhost

#. VRRP协议

   Virtual Router Redundancy Protocol implementation (VRRPv3)
   
   - VRRPv3 (RFC 5798) for IPv4 and IPv6
     - Signaling/advertisements and election of a master
     - Replies to ARP, NS requests for virtual router addresses
   
   - VRRP virtual MAC address support
     - DPDK interfaces with PMD support for multiple MAC addresses via the rte_eth_dev_mac_addr_add(), rte_eth_dev_mac_addr_del()
     - Other interfaces which are set in promiscuous mode may work
   
   - Support interface types for VRRP virtual routers
     - Hardware interfaces
     - VLAN subinterfaces
     - Bond interfaces
   
   - Additional features not specified in RFC 5798
     - Allows sending advertisements to unicast peers instead of multicast
     - Allows a virtual router's priority to be adjusted based on the state of an upstream interface. Mentioned as a configuration option to "track interfaces or networks" in RFC 8347.
    
#. VxLAN 

   Virtual eXtensible LAN (VXLAN) tunnels support L2 overlay networks that span L3 networks
   
     - VXLAN tunnel for support of L2 overlay/virtual networks (RFC-7348)
     - Support either IPv4 or IPv6 underlay network VTEPs
     - Flooding via headend replication if all VXLAN tunnels in BD are unicast ones
     - Multicast VXLAN tunnel can be added to BD to flood via IP multicast
     - VXLAN encap with flow-hashed source port for better underlay IP load balance
     - VXLAN decap optimization via vxlan-bypass IP feature on underlay interfaces
     - VXLAN decap HW offload using flow director with DPDK on Intel Fortville NICs

#. VxLAN-GPE 

   VxLAN-GPE tunnel handling
   
     - VxLAN-GPE decapsulation
     - VxLAN-GPE encapsulation

#. Wireguard协议

   Wireguard协议实现
     
     - based on wireguard-openbsd implementation: https://git.zx2c4.com/wireguard-openbsd
     - creating secure VPN-tunnel

#. arping

   arping command
     
     - arping command to send either gratuitous or ARP request to the remote
     - support both IPv4 and IPv6

#. host-interface - AF_PACKET

   Create a host interface that will attach to a linux AF_PACKET interface, one side of a veth pair. The veth pair must already exist. Once created, a new host interface will exist in VPP with the name 'host-<ifname>', where '<ifname>' is the name of the specified veth pair. Use the 'show interface' command to display host interface details.
     
     - L4 checksum offload
     - GSO offload
 
#. IKEv2

   Internet Key Exchange (IKEv2) Protocol plugin

     - RFC 7296 "Internet Key Exchange Protocol Version 2 (IKEv2)"
     - NAT-T, ESN, PSK and public key authentication
     - AES-CBC-128/192/256 and AES-GCM-16-128/192/256 encryption
     - HMAC-SHA2-256/384/512 and HMAC-SHA1 pseudo-random functions
     - HMAC-SHA2-256-128/384-192/512-256 integrity
     - MODP and ECP Diffie-Hellman

#. RDMA设备驱动 

   rdma device driver support

     - rdma driver for Mellanox ConnectX-4/5 adapters

#. vlib/unix功能集

   Linux-specific support routines
     
     - epoll-based file I/O support
     - Linux signal handling
     - Syslog support
     - Startup configuration processing, logging
     - Configuration debug CLI pager, banner, main loop polling interval
     - Per-thread stack allocation, guard-page setup

#. vmxnet3设备驱动

   vmxnet3 device driver support

     - vmxnet3 device driver to connect to ESXi server, VMWare Fusion, and VMWare Workstation
     - Supports GSO. It was tested on ESXi 6.7

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ipsec
   vxlan
   classify
