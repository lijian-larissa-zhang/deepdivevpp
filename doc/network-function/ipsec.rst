IPSec
=====

功能介绍
--------------
IPsec（Internet Protocol Security）作为网络安全的一个重要协议，其实现的功能体现了网络安全的大部分需求，覆盖了网络安全领域的主要概念。IPsec主要依赖密码技术提供认证和加密机制，它是现代密码技术在通信领域的应用范例。同时，因为IPsec为用户数据提供了一个安全通路，也被视为VPN技术的一个分支。

IPsec并非一个单独的协议，而是一系列为IP网络提供完整性的协议和服务的集合。这些服务和协议结合起来提供不同类型的保护。因为IPsec工作在IP层，所以他能为上层协议和应用提供透明的安全服务，这也是他最大的优势。

IPsec常用缩略语
~~~~~~~~~~~~~~~~~~~~
.. list-table:: IPsec常用缩率语
   :widths: 10 40
   :header-rows: 1

   * - 缩率语
     - 说明
   * - AH
     - IP Authentication Header：IP认证头协议，提供数据完整性保护
   * - ESP
     - IP Encapsulating Security Payload：IP封装载荷协议，提供数据私密性和完整性保护
   * - SA
     - Security Association：安全关联，包含加解密服务相关参数的结构，比如算法类型、密钥、封装信息等
   * - SAD
     - SA Database：安全关联数据库
   * - SP
     - Security Policy：安全策略，用来匹配流量信息，指明对应的流量动作
   * - SPD
     - SP Database：安全策略数据库
   * - SPI
     - Security Parameters Index：安全参数索引，携带在AH或ESP报头中的32bit的数值，用来在接收端标识数据流到SA的绑定关系（实际使用中，SA的索引可能是SPI+源IP+目的IP 或者 SPI+目的IP）
   * - IKE
     - Internet Key Exchange，Internet 密钥交换协议，希望为Internet上任何需要加密和认证的通信协议提供算法和密钥协商的服务

IPsec整体框架
~~~~~~~~~~~~~~~~~~~~
IPsec整体框架如下图所示

.. figure:: ../images/ipsec/ipsec_arch.png
   :align: center

   IPsec整体框架图

IPsec包含三部分：密码学（认证算法、加密算法、AEAD算法）、密钥管理、IPsec封装协议（AH协议、ESP协议），通过这三部分，可以提供以下安全保护：
 * 用户数据加密，通过加密算法提供数据私密性；
 * 消息完整性验证，通过摘要认证确保数据在传输路径上未经修改；
 * 防御特定类型的攻击，通过序列号防数据重放，通过认证防中间人攻击；
 * 提供隧道和传输两种安全模式，满足不同的网络结构需求。
 * 提供设备间安全算法和密钥的协商能力，提供安全的在线密钥生成机制

IPsec的协议封装
~~~~~~~~~~~~~~~~~~~~
IPsec协议封装有两种模式（隧道模式和传输模式），两种协议（AH协议和ESP协议）。隧道模式除了做AH或ESP报文封装外，还会在原始报文外再封装一层新的IP头，传输模式则仅在原始报文中封装AH或ESP协议信息，具体封装信息参见对应的RFC4302、RFC4303。

实现机制
------------
vnet中的IPsec实现基于以下RFC，注：并未在vnet中实现IKE以及加解密算法：
 * https://tools.ietf.org/html/rfc4301
 * https://tools.ietf.org/html/rfc4302
 * https://tools.ietf.org/html/rfc4303

在vnet中，实现了两种模式的IPsec VPN，一种是基于路由的VPN，一种是基于策略的VPN [1]_。

基于路由的VPN
~~~~~~~~~~~~~~~~~~~~
基于路由的VPN是指所有到某一特定对等体的数据包都由同一SA加密（实际上是一对SA；一个用于入向和出向），路由决定将流量转发到哪个对等体。因此，这种方式是通过路由选择SA。当然，反过来也是一样的，即所有来自特定对等体的数据包都用相同的安全协议进行解密。

一种典型的实现这种受保护的对等体的方式是使用一个点对点的虚拟接口，对等体被连接到该接口上，SA对被关联到该虚拟接口。需要保护的前缀通过这个虚拟接口进行路由，从而保护到对等体的流量。

该模型有三个组成部分：SAs，虚拟接口，SAs与虚拟接口的关联（VPP中称之为the protection），在VPP中，SAs与虚拟接口的关联（为表述方便，下文中称为Protection）由ipsec_tun_protect_t表示。其中，tun部分表示被保护的这个虚拟接口通常是一个隧道接口。每个虚拟接口可以关联一个出向SA以及最多四个入向SA。之所以可以关联四个SA是因为以下原因：
 * 保证cache-line的对齐
 * 在重协商阶段，对等体可以先添加新的入向SA，再延迟更新出向SA，然后再延迟一端时间删除旧的入向SA，这样可以最大限度地减少或消除数据包丢失
虚拟接口可以表达为以下两种方式：
 * 接口+封装+SA = （接口+封装）+SA = ipip-interface +SA传输模式
 * 接口+封装+SA = 接口+（封装+SA） = IPsec-interface +SA隧道模式

如何使用上面的方式，取决于具体的用户配置。对于基于路由的VPN，可能会有0层封装头、1层封装头、2层封装头，其组合如下表所示：

.. list-table:: 基于路由的VPN配置组合
   :widths: 10 10 10 10
   :header-rows: 1

   * -
     - 0
     - 1
     - 2
   * - ipip
     - N
     - Y
     - Y
   * - ipsec
     - P
     - Y
     - P

P = potentially

当接口和SA都不需要隧道封装模式时，IPsec有可能支持0层封装头（即传输模式）。然而，对于基于路由的VPN来说，不能使用传输模式，因为没有外层封装头，传输的流量就不能保证返回。IPsec有可能支持两个封装头，但这需要SA描述这两个封装，目前它还没有这样做。

在VPP内部上，区别在于IPsec接口的中间链邻接没有相关的封装信息（对于ipip隧道，有封装信息，并描述了对等体）。因此，从输出上的看，没有任何带外层封装的数据包。由于SA指示了隧道模式的封装，因此只有当知道SA时，才会知道对等体，链中的邻接信息才会被迭代才会被堆叠。在其他的方面，其的VLIB图节点是相同的。

node节点图如下：

.. figure:: ../images/ipsec/node.png
   :align: center

   IPsec node节点图

图中 X = 4 or 6。
使用IPsec接口的效率略高，因为封装的IP头的校验和只更新一次。

当接口处于管理状态时，除非SA可用，否则不能向对等体发送流量（因为是SA决定了封装信息）。对于ipip接口，如果SA不可用，必须使用管理状态来防止发送。

对以上两种模型的使用，VPP作者提供的建议是：
 * 选择一个支持你的使用情况的模型
 * 确保你想使用的任何其他功能都被该模型所支持
 * 选择最适合你的控制平面模型的模型

点到多点接口
>>>>>>>>>>>>>>>>>>
如上所述，基于路由的VPN用相同的SA对保护所有目的地为某一特定对等体的数据包。这种保护是用一个虚拟的P2P接口来模拟的，所以我们可以合理地认为，所有通过该接口的流量都受到SA对的保护，或者所有到对等体的流量都受到保护，因为它们是一体的。然而，当我们考虑点到多点接口时，我们必须考虑保护适用于链接上的对等体。

当在P2MP链路上使用IPsec保护时，ipsec_tun_protect_t 将指定一个特定的对等体（在P2P情况下，这个对等体是通常的特殊全零地址，也就是这个虚接口对应的对等体的IP地址用全零来表示）。使用基于路由的VPN的所有其他方面仍然是一样的。路由是通过接口上的特定对等体迭代解析的，即：
ip route add 10.0.0.0/8 via 192.168.1.1 mipip0

对于一个多接入的接口，路由应该总是指明一个下一跳。无论是P2P还是P2MP，数据路径是不变的，出向的SA来自邻接关系，入向的SA通过SPI和接口相匹配。

基于策略的VPN
~~~~~~~~~~~~~~~~~~~~
在基于策略的VPN中，SA是根据特定的IPsec策略来选择的，策略描述了要匹配数据包的属性，以及匹配后要采取哪些动作。动作包括：
 * 绕过：忽略它
 * 丢弃：丢弃它
 * 保护：使用特定的SA进行加密或解密。

目前VPP不支持（根据RFC4301）当匹配不到SA时，启动IKE会话的 “解析”操作。它按照RFC4301中描述的IPsec实现方式，策略被存储在安全策略数据库（SPD）中，一个SPD关联到一个接口。进出接口的数据包与关联的SPD中的策略进行匹配。

关键数据结构
~~~~~~~~~~~~~~~~~~~~
SAD与SPD
>>>>>>>>>>>>>>>>>>
如上文所述，IPsec中需要实现两个关键数据结构：SPD与SAD，VPP中针对SPD和SAD的数据关系如下图所示：

.. figure:: ../images/ipsec/spd_sad.png
   :align: center

   SPD与SAD关系图

在VPP通过ipsec_main维护IPsec模块的管理全局管理数据结构，在ipsec_main_t这个结构体中，存有SPD的vec向量指针spds以及SP的vec向量指针polices，其中spd可以通过spd_id或sw_if_index作为键值做hash查找。
SAD通过一个全局的pool向量ipsec_sa_pool来实现存储，其在ipsec_main中也有对应的hash表的数据实现，其hash的键值为sa_id。

节点关系图
>>>>>>>>>>>>>>>>>>
本节主要描述IPsec中节点之间的关系图，重点描述了业务特性相关的节点，对于异步加解密的流程没有体现。
根据实现机制的类型，节点关系分为两大块：基于隧道虚接口机制的节点关系图和基于策略绑定接口的节点关系图。每种关系图中，又分为入向和出向两个流程。

基于隧道虚接口机制的入向节点关系图
:::::::::::::::::::::::::::::::::::::::::::::

.. figure:: ../images/ipsec/tunnel_input_node.png
   :align: center

   基于隧道虚接口机制的入向节点关系图

基于隧道虚接口机制的出向节点关系图
:::::::::::::::::::::::::::::::::::::::::::::

.. figure:: ../images/ipsec/tunnel_output_node.png
   :align: center

   基于隧道虚接口机制的入向节点关系图

基于策略绑定接口的入向节点关系图
:::::::::::::::::::::::::::::::::::::::::::::

.. figure:: ../images/ipsec/interface_policy_input_node.png
   :align: center

   基于策略绑定接口的入向节点关系图

基于策略绑定接口的出向节点关系图
:::::::::::::::::::::::::::::::::::::::::::::

.. figure:: ../images/ipsec/interface_policy_output_node.png
   :align: center

   基于策略绑定接口的出向节点关系图

代码分析
~~~~~~~~~~~~~~~~~~~~
创建SA
>>>>>>>>>>>>>>>>>>
控制面通过CLI或者API调用此函数创建SA，其函数原型如下：

.. code:: c

	int
	ipsec_sa_add_and_lock (u32 id, u32 spi, ipsec_protocol_t proto, ipsec_crypto_alg_t crypto_alg, 
		const ipsec_key_t *ck, ipsec_integ_alg_t integ_alg, const ipsec_key_t *ik, 
		ipsec_sa_flags_t flags, u32 salt, u16 src_port, u16 dst_port, 
		const tunnel_t *tun, u32 *sa_out_index)

入参中id是用户指定的SA的id，用来作为查找sa的hash索引；spi是SA在SAD中的键值之一；proto指明具体的IPsec协议是ESP还是AH，crypto_alg指明加密算法，ck为加密密钥；integ_alg指明认证算法，ik未认证密钥；flags指明IPsec封装的一些标记；salt指明某些算法中需要的随机数；src_port和dst_port指明隧道封装时的源端口和目的端口号；tun指明了隧道信息；sa_out_index是个出参，指明SA在ipsec_sa_pool中的索引。该函数主要步骤如下
 * 从ipsec_sa_pool中申请一个SA，将入参中相关的信息保存到申请的SA中
 * 初始化SA的thread信息，初始时并未分配thread给SA
 * 根据指定的算法设置对应的加解密库需要的信息
 * 如果是隧道模式的出向SA，则会生成相关的dpo信息，并提前组装对应的外层IP头
 * 如果开启了udp-encap，则会设置udp端口信息，对于入向SA还会向VPP的UDP模块注册对应的目的端口
 * 将SA在ipsec_sa_pool中的偏移值以id为键值保存到ipsec_main中的sa_index_by_sa_id中

创建SPD
>>>>>>>>>>>>>>>>>>
控制面通过CLI或者API调用此函数创建SP，其函数原型如下：

.. code:: c

	int 
	ipsec_add_del_spd (vlib_main_t * vm, u32 spd_id, int is_add)

入参中spd_id指明SPD在SPD中的id，is_add指明是添加还是删除。本函数比较简单，主要是从ipsec_main_t的spds中申请一个SPD，并将其通过hash索引起来

创建SP
>>>>>>>>>>>>>>>>>>
控制面通过CLI或者API调用此函数创建SP，其函数原型如下：

.. code:: c

	int
	ipsec_add_del_policy (vlib_main_t * vm, ipsec_policy_t * policy, int is_add, u32 * stat_index)

入参中policy指明具体的policy信息，比如流的五元组等；is_add指明是添加还是删除；出参stat_index指明SP在策略池中的申请索引。

接口绑定SPD
>>>>>>>>>>>>>>>>>>
控制面通过CLI或者API调用此函数为接口绑定SPD，其函数原型如下：

.. code:: c

	int
	ipsec_set_interface_spd (vlib_main_t * vm, u32 sw_if_index, u32 spd_id, int is_add)

入参中sw_if_index指明了需要绑定SPD的接口；spd_id指明了接口需要绑定的SPD的id；is_add指明是绑定还是解绑。
该函数主要是为接口开启IPsec功能，通过 vnet_feature_enable_disable 函数在发送方向的ip4-output中开启ipsec4-output-feature，在ip6-output 中开启 ipsec6-out-feature；在接收方向的ip4-unicast中开启ipsec4-input-feature，在ip6-unicastut 中开启 ipsec6-out-feature

创建IPsec隧道
>>>>>>>>>>>>>>>>>>
控制面通过CLI或者API调用此函数创建IPsec虚接口，其函数原型如下：

.. code:: c

	int
	ipsec_itf_create (u32 user_instance, tunnel_mode_t mode, u32 * sw_if_indexp)

入参user_instance是用户指定的实例号，如果用户不指定则系统自动分配；入参mode指明隧道的模式，默认为P2P模式；出参sw_if_indexp指向接口创建后在接口模块中申请到的索引号
 * 从ipsec_itf_instance中分配实例号
 * 从ipsec_itf_pool中分配ipsec_itf_t
 * 向接口模块注册接口，并获取到接口索引号
 * 将隧道索引通过全局变量ipsec_itf_index_by_sw_if_index数组来索引，对应下表为接口索引号，即建立隧道接口索引和隧道索引之间的对应关系
 * 重设置IPsec隧道虚接口的发送节点信息，默认为ipX-drop和mpls-drop


IPsec入向节点
>>>>>>>>>>>>>>>>>>
IPsec入向处理节点处理函数分别为ipsec4_input_node_fn和ipsec6_input_node_fn，其处理逻辑实现了基于策略的VPN的入向处理，其大致逻辑如下：
 * 从feature中获取到接口的spd_index
 * 根据spd_index从ipsec_main_t中找到对应的SPD
 * 获取到报文中的SPI，并调用ipsec_input_protect_policy_match对报文进行匹配，主要匹配了SPI和IP地址
 * 如果没有匹配到protect在会调用ipsec_input_policy_match去匹配bypass和discard的SP
 * 根据匹配的接口设置next的node信息，如果是需要解密的，还会把从SP中匹配到的sa_index放在报文的元数据区
 * 根据next信息进入下一个node流程

IPsec出向节点
>>>>>>>>>>>>>>>>>>
IPsec出向处理节点处理函数分别为ipsec4_output_node_fn和ipsec6_output_node_fn，其处理逻辑实现了基于策略的VPN的出向处理，其大致逻辑如下：
 * 根据出接口的接口索引号从ipsec_main_t中的hash表中查找到spd_index
 * 根据spd_index从ipsec_main_t中找到对应的SPD
 * 根据SP去匹配报文策略，如果是protect则会将sa_index放在报文的元数据去，并进入加密流程，否则继续其他对应的流程

加密
>>>>>>>>>>>>>>>>>>
根据IP协议类型以及封装协议类型，加密有以下节点：ah4_encrypt_node，ah6_encrypt_node，esp4_encrypt_node，esp6_encrypt_node，其大致的逻辑是类似的：
 * 获取sa_index，获取方式和是否是隧道接口有关
 * 处理encrypt_thread_index，如果未赋值则分配加密线程，如果已赋值则会根据分配的线程信息判断是否要handoff
 * 统计信息处理
 * 根据协议信息以及封装模式做对应的报文头处理
 * 根据是否是隧道口以及封装模式设置next节点的索引
 * 调用对应的加密处理函数做对应的加密或认证
 * 调用vlib_buffer_enqueue_to_next转交给下一个节点处理

解密
>>>>>>>>>>>>>>>>>>
根据IP协议类型以及封装协议类型，加密有以下节点：ah4_decrypt_node，ah6_decrypt_node，esp4_decrypt_node，esp6_decrypt_node，其大致的逻辑是类似的：
 * 从报文的元数据中获取到sa_index，并找到SA
 * 处理decrypt_thread_index，如果未赋值则分配解密线程，如果已赋值则会根据分配的线程信息判断是否要handoff
 * 抗重放处理
 * 统计信息处理
 * 调用对应的解密处理函数做对应的解密或认证
 * 根据SA的标记，设置下一个节点，隧道接口和接口绑定SP的处理是不同的
 * 调用vlib_buffer_enqueue_to_next转交给下一个节点处理

配置与命令
------------

网络拓扑
~~~~~~~~~~~~~~~~~~~~
.. figure:: ../images/ipsec/ipsec_topology.png
   :align: center

   IPsec实验拓扑


下文以如上拓扑给出几个IPsec的配置示例。该拓扑中有2个Host，每个Host上启用一个VPP实例，使用默认的VPP启动配置脚本。
每个Host内创建一对veth口，对应的IP设置如上所示，其与VPP间通过veth口通信；VPP间通过eth口通信。
Host1配置如下：

.. code-block:: console

    ip link add name vpp1out type veth peer name vpp1host
    ip link set dev vpp1out up
    ip link set dev vpp1host up
    ip addr add 10.0.1.1/24 dev vpp1host
    ip route add 10.0.2.0/24 dev vpp1host via 10.0.1.2

VPP1配置如下：

.. code-block:: console

    set interface state GigabitEthernet2/5/0 up
    set interface ip address GigabitEthernet2/5/0 192.168.1.1/24
    create host-interface name vpp1out
    set interface state host-vpp1out up
    set interface ip address host-vpp1out 10.0.1.2/24

Host2配置如下：

.. code-block:: console

    ip link add name vpp2out type veth peer name vpp2host
    ip link set dev vpp2out up
    ip link set dev vpp2host up
    ip addr add 10.0.2.1/24 dev vpp2host
    ip route add 10.0.1.0/24 dev vpp2host via 10.0.2.2

VPP2配置如下：

.. code-block:: console

    set interface state GigabitEthernet2/5/0 up
    set interface ip address GigabitEthernet2/5/0 192.168.1.2/24
    create host-interface name vpp2out
    set interface state host-vpp2out up
    set interface ip address host-vpp2out 10.0.2.2/24

命令介绍
~~~~~~~~~~~~~~~~~~~~
IPsec相关的命令主要包括：

.. list-table:: IPsec命令介绍
   :widths: 10 40
   :header-rows: 1

   * - 简介
     - 帮助信息
   * - 创建或删除SA
     - ipsec sa {add|del} <id> {spi <spi>} {esp | ah} [salt <0x%x>] [crypto-key <key>] [crypto-alg <alg>] [integ-key <key>] [integ-alg <alg>] [udp-src-port <port>] [udp-dst-port <port>] [inbound] [use-esn] [udp-encap] [async] [src <ip>] [dst <ip>] [table-id <id>] [hot-limit <num>] [<tunnel-encap-flag>] [<dscp-flag>] [<tunnel-mode-flag>]
   * - 创建或删除SPD
     - ipsec spd [add|del] <id>
   * - 创建或删除SP
     - ipsec policy [add|del] spd <id> priority <n> [ip6] inbound | outboud } [protocol <num>] action <action> [sa <sa-id>] [local-ip-range <ip>-<ip>] [remote-ip-range <ip>-<ip>] [local-port-range <port>-<port>] [remote-port-range <port>-<port>]
   * - 接口绑定SPD
     - set interface ipsec spd [del] <interface> <spd-id>
   * - 显示所有的IPsec信息
     - show ipsec all
   * - 显示所有IPsec SA或指定SA的信息
     - show ipsec sa [index]
   * - 清除所有IPsec SA或指定SA
     - clear ipsec sa [index]
   * - 显示所有IPsec SPD或指定SPD的信息
     - show ipsec spd [index]
   * - 显示IPsec的处理后端模块
     - show ipsec backends
   * - 指定IPsec的后端处理模块
     - ipsec select backend <ah|esp> <backend index>
   * - 显示IPsec虚接口
     - show ipsec tunnel
   * - 清除IPsec的统计计数
     - clear ipsec counters
   * - 为IPsec隧道绑定或解除SA
     - ipsec tunnel protect <interface> sa-in <SA> sa-out <SA> [add|del]
   * - 显示IPsec隧道的保护信息
     - show ipsec protect
   * - 显示IPsec隧道的保护信息，主要显示隧道的Hash键值
     - show ipsec protect-hash
   * - 创建IPsec隧道虚接口
     - ipsec itf create [instance <instance>]

- ipsec sa

  + add | del ：指明是创建还是删除SA
  + id：必选，SA对应的id，会通过该id创建对应的hash条目
  + spi <spi>：必选，SA的spi，可以通过该值创建对应的hash条目
  + esp | ah：必选，指明SA的封装协议
  + salt <0x%x>：某些加密算法需要的随机数信息
  + crypto-key <key>：指明加密算法的密钥，该密钥以16进制形式的字符串输入，长度为算法指明的长度
  + crypto-alg <alg>：指明加密算法的类型，以固定的字符串输入
  + integ-key <key>：指明认证算法的密钥，该密钥以16进制形式的字符串输入，长度为算法指明的长度
  + integ-alg <alg>：指明认证算法的类型，以固定的字符串输入
  + udp-src-port <port>：指明使用udp-encap封装时的udp源端口号
  + udp-dst-port <port>：指明使用udp-encap封装时的udp目的端口号
  + udp-encap：指明使用udp-encap封装，即IPsec协议头前再封装一层UDP头，以用来支持NAT穿越
  + async：SA使用异步方式做加解密
  + inbound：指明SA为入向SA
  + use-esn：指明使用esn，即64bits的序列号
  + 隧道封装信息：

    * src <ip>：隧道源IP
    * dst <ip>：隧道目的IP
    * table-id <id>：隧道封装后查找路由的路由表id
    * hot-limit <num>：隧道头的ttl信息
    * tunnel-encap-flag：隧道的封装标记，包含很多中，比如从内层头中拷贝df标记等，具体参见tunnel.h
    * tunnel-mode-flag：隧道模式类型标记，如p2p和p2mp
    * dscp-flag：隧道头的dscp标记，参见ip.c

- ipsec spd

  + add | del ：指明是创建还是删除SPD
  + id：必选，SPD对应的id，会通过该id创建对应的hash条目

- ipsec policy 

  + add | del：指明是创建还是删除SP
  + spd <id>：指明策略所属的SPD的id
  + priority <n>：指明策略的优先级
  + inbound | outbound：指明策略的方向
  + protocol <num>：指明策略的五元组中的协议号
  + action：指明策略动作，有bypass，discard，reslove，protect四种
  + sa <sa-id>：指明SP对应的SA的id
  + local-ip-range <ip>-<ip>：指明策略的五元组中的源地址范围
  + remote-ip-range <ip>-<ip>：指明策略的五元组中的目的地址范围
  + local-port-range <port>-<port>：指明策略的五元组中的源端口范围
  + remote-port-range <port>-<port>：指明策略的五元组中的目的端口范围

- set interface ipsec spd

  + del：指明是否需要将接口上的SPD解除绑定
  + interface：指明需要绑定或解除绑定的接口名称
  + spd-id：指明需要绑定或解除绑定的SPD的id

- show ipsec sa

  + index：显示指定索引的IPsec SA的信息

- clear ipsec sa

  + index：清除指定索引的IPsec SA

- show ipsec spd
  + index：显示指定索引的IPsec SPD的信息

- show ipsec

  + backends：显示实际实现ESP或AH的后端处理模块，在历史版本中，ESP和AH协议可以由VPP内部的模块实现，也可以选用DPDK的模块实现

- ipsec select backend

  + ah | esp：指明选用哪种协议的后端设备
  + backend index：后端设备的索引

- ipsec tunnel protect

  + <interface>：指明IPsec隧道口名字
  + sa-in <SA>：指明绑定的入向SA，参数为SA的id
  + sa-out <SA>：指明绑定的出向SA，参数为SA的id
  + [add | del]：指明是为IPsec接口绑定还是接绑定SA

- ipsec itf create

  + instance <id>：IPsec虚接口实例号

配置实例
~~~~~~~~~~~~~~~~~~~~
IPsec虚接口+隧道SA
>>>>>>>>>>>>>>>>>>
拓扑图如上文所示。
VPP1其他配置如下：
创建IPsec SA：

.. code-block:: console

    ipsec sa add 30 spi 2001 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653769 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58 tunnel src 192.168.1.2 dst 192.168.1.1
    ipsec sa add 40 spi 2002 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653770 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58 tunnel src 192.168.1.1 dst 192.168.1.2

使能接口的IPv4功能，并设置通往对端的路由：

.. code-block:: console

    set interface unnumbered ipsec1 use GigabitEthernet2/5/0
    ip route add 10.0.2.0/24 via ipsec1

为IPsec虚接口绑定SA：

.. code-block:: console

    ipsec tunnel protect ipsec1 sa-in 30 sa-out 40

VPP2其他配置如下：
创建IPsec SA：

.. code-block:: console

    ipsec sa add 30 spi 2001 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653769 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58 tunnel src 192.168.1.2 dst 192.168.1.1
    ipsec sa add 40 spi 2002 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653770 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58 tunnel src 192.168.1.1 dst 192.168.1.2

使能接口的IPv4功能，并设置通往对端的路由：

.. code-block:: console

    set interface unnumbered ipsec1 use GigabitEthernet2/5/0
    ip route add 10.0.1.0/24 via ipsec1

为IPsec虚接口绑定SA：

.. code-block:: console

    ipsec tunnel protect ipsec1 sa-in 40 sa-out 30

配置结束后，可以在Host1测通过ping命令进行测试：

.. code-block:: console

    ping 10.0.2.1 -c 4
    PING 10.0.2.1 (10.0.2.1) 56(84) bytes of data.
    64 bytes from 10.0.2.1: icmp_seq=1 ttl=62 time=2.43 ms
    64 bytes from 10.0.2.1: icmp_seq=2 ttl=62 time=1.17 ms
    64 bytes from 10.0.2.1: icmp_seq=3 ttl=62 time=2.32 ms
    64 bytes from 10.0.2.1: icmp_seq=4 ttl=62 time=3.28 ms

同时，可以通过在vppctl中开启trace查看报文情况：

.. code-block:: console

    trace add af-packet-input 1000
    trace add dpdk-input 1000

物理口+传输SA
>>>>>>>>>>>>>>>>>>
拓扑图如上文所示。
VPP1其他配置如下：
创建IPsec SA：

.. code-block:: console

    ipsec sa add 10 spi 2001 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653768 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58

创建SPD和SP

.. code-block:: console

    ipsec spd add 1
    ipsec policy add spd 1 priority 10 outbound action protect sa 10 local-ip-range 10.0.1.1 - 10.0.1.255 remote-ip-range 10.0.2.1 - 10.0.2.255
    ipsec policy add spd 1 priority 10 intbound action protect sa 10 local-ip-range 10.0.1.1 - 10.0.1.255 remote-ip-range 10.0.2.1 - 10.0.2.255

物理口绑定SP：

.. code-block:: console

    set interface ipsec spd GigabitEthernet2/5/0 1

配置对端路由

.. code-block:: console

    ip route add 10.0.2.0/24 via 192.168.1.2

VPP2其他配置如下：
创建IPsec SA：

.. code-block:: console

    ipsec sa add 10 spi 2001 esp crypto-alg aes-cbc-128 crypto-key 4a506a794f574265564551694d653768 integ-alg sha1-96 integ-key 4339314b55523947594d6d3547666b45764e6a58

创建SPD和SP

.. code-block:: console

    ipsec spd add 1
    ipsec policy add spd 1 priority 10 inbound action protect sa 10 local-ip-range 10.0.2.1 - 10.0.2.255 remote-ip-range 10.0.1.1 - 10.0.1.255
    ipsec policy add spd 1 priority 10 outbound action protect sa 10 local-ip-range 10.0.2.1 - 10.0.2.255 remote-ip-range 10.0.1.1 - 10.0.1.255

物理口绑定SP：

.. code-block:: console

    set interface ipsec spd GigabitEthernet2/5/0 1

配置对端路由

.. code-block:: console

    ip route add 10.0.1.0/24 via 192.168.1.1

参考
----
.. [1] https://s3-docs.fd.io/vpp/22.02/developer/corefeatures/ipsec.html?highlight=ipsec