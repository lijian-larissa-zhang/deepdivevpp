流分类器 - Classifier
=====================

VPP有若干模块，都能提供访问控制列表（ACL - Access Control List)功能。这些模块各具特色 [1]_。

 * ACL Plugin - 严格匹配报文五元组；具有无状态和有状态功能；无状态ACL性能最低
 * VPP COP - 匹配报文IP地址；无状态
 * VPP Flow - 硬件加速方案；严格匹配报文五元组；可配置的报文流数目有硬件限制
 * Classifiers - 能够匹配报文特定起始位置的80字节内的任意域；性能相对无状态ACL要好

本章主要介绍流分类器 - Classifier的功能特点、实现机制和使用案例。

功能介绍
--------------

流分类器实现和ACL类似的功能，但是流分类器在匹配报文域和执行匹配命中报文流的动作上，具有更灵活的特性。流分类器能够匹配报文特定区间80个字符之内的任意域。该80字节报文区间由 ``offset`` ``length`` ``mask`` 三个元素决定。 ``offset`` 决定报文区间的起始位置；为利用SIMD指令进行快速报文匹配，起始位置需要16字节对齐。 ``length`` 决定区间的长度，长度以16字节整倍数；长度最长为5个16字节，共计80字节。 ``mask`` 决定区间内有效的比特位域。

.. figure:: ../images/classify/classify_olm.png
   :align: center

   流分类器匹配域

举例说明流分类器工作原理。如下所示报文，目的MAC地址为 ``4c:e1:76:1a:7b:1f`` ，源MAC地址为 ``34:48:ed:f8:59:af`` ，没有VLAN tag，以太网类型0x0800的IP报文，源IP地址为10.169.210.133（0x0aa9d285），目的IP地址为10.169.244.159（0x0aa9f49f）。

::

    0x0000:  4ce1 761a 7b1f 3448 edf8 59af 0800 4510
    0x0010:  0154 8af2 4000 4006 d22a 0aa9 d285 0aa9
    0x0020:  f49f 0016 e183 a249 a543 3494 6bfe 5018
    0x0030:  000b ddbd 0000 9794 cc0d 018b c566 b6b9
    0x0040:  e10e 3286 a6de ce7c 911a ad13 b367 898a
    0x0050:  ...

1. 若设计流分类器，只匹配报文源MAC地址，那么流分类器的区间设置只需要覆盖报文第一个16字节就可以了。流分类器报文区间定义三元素分别为： ``offset`` 设置为0， ``length`` 设置为16， ``mask`` 设置为 ::

    0x0000:  0000 0000 0000 ffff ffff ffff 0000 0000

匹配上述报文流，需要向流分类器中添加如下关键字会话条目，以匹配命中报文流，之后执行用户指定的动作 ::

    0x0000:  0000 0000 0000 3448 edf8 59af 0000 0000

2. 若设计流分类器，需要匹配报文源IP和目的IP地址，那么流分类器的区间设置需要覆盖第二个和第三个16字节。流分类器报文区间定义三元素分别为： ``offset`` 设置为16， ``length`` 设置为32， ``mask`` 设置为 ::

    0x0000:  0000 0000 0000 0000 0000 ffff ffff ffff
    0x0010:  ffff 0000 0000 0000 0000 0000 0000 0000

匹配上述报文流，需要向流分类器中添加如下关键字会话条目，以匹配命中报文流，之后执行用户指定的动作 ::

    0x0000:  0000 0000 0000 0000 0000 0aa9 d285 0aa9
    0x0010:  f49f 0000 0000 0000 0000 0000 0000 0000

3. 若设计流分类器，需要匹配报文源MAC地址、报文源IP和目的地址，那么流分类器的区间设置需要覆盖前三个16字节。流分类器报文区间定义三元素分别为： ``offset`` 设置为0， ``length`` 设置为48， ``mask`` 设置为 ::

    0x0000:  0000 0000 0000 ffff ffff ffff 0000 0000
    0x0010:  0000 0000 0000 0000 0000 ffff ffff ffff
    0x0020:  ffff 0000 0000 0000 0000 0000 0000 0000

匹配上述报文流，需要向流分类器中添加如下关键字会话条目，以匹配命中报文流，之后执行用户指定的动作 ::

    0x0000:  0000 0000 0000 3448 edf8 59af 0000 0000
    0x0010:  0000 0000 0000 0000 0000 0aa9 d285 0aa9
    0x0020:  f49f 0000 0000 0000 0000 0000 0000 0000

上述 ``offset`` ``length`` ``mask`` 的计算、设置过程由软件自动完成，用户只需要在命令行，通过命令配置需要关注的报文域即可，比如

.. code-block:: console

    vpp$ classify table acl-miss-next permit mask l3 ip4 src buckets 16
    vpp$ classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.1
    vpp$ classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.2

上述例子中，第一条命令首先定义流分类器，指定流分类器匹配报文的源IP域，流分类器默认允许报文转发。第二条和第三条命令各向流分类器中添加一条报文流会话条目，分别匹配源IP为192.81.0.1和192.81.0.2的报文流。一旦匹配命中，执行丢弃报文行为。未命令的报文流，执行流分类器默认的行为：允许转发。

实现机制
--------------

首先关注若干流分类器模块中涉及到的术语 [2]_。

* 流分类器 - Classifier
    报文流分类器，通过 ``offset`` ``length`` ``mask`` 定义一段小于等于80字节的区间。该区间关注报文特定的域，比如典型的报文五元组域，以太网帧头域，或者深入应用层字段的任意域。

* 流分类器表 - Classifier Table
    针对某一流分类器的软件实现。除了通过 ``offset`` ``length`` ``mask`` 字段定义待匹配的报文域，流分类表还包括匹配特定报文流的键值对： ``键`` 指示通信会话的报文流内容， ``值`` 指示一旦匹配命中该报文流会话，流分类器所采取的动作。

* 流分类器会话条目 - Classifier Session
    也即报文流会话，流分类器表中包含的条目，代表一个具体报文流、会话。

* 流分类器有序表
    若干流分类器表可以串联起来，构成有序表结构，有序表的数据元素就是一个流分类器。

流分类器在设计之初，主要使用128位长度的intel MMX和Arm NEON指令加速报文匹配和哈希计算。这解释了 ``offset`` 以16字节对齐， ``length`` 为16字节整倍数的来源。截至vpp v22.02，流分类器已经增加了256位、512位的SIMD指令代码，对报文匹配和哈希计算进行加速。

流分类器表包含一系列会话条目。在报文匹配过程中，按顺序遍历流分类器有序表中的每一个流分类器。读取每一个流分类器的掩码 ``offset`` ``length`` ``mask`` ，与报文内容进行与操作，取得报文关键值。根据关键值，在流分类器哈希表中查找匹配的会话条目，如果找到匹配的规则，它将采取会话条目中指示的动作；如果没有找到匹配的规则，则采取流分类器默认的动作。

流分类器提供灵活的报文匹配功能。仔细设计流分类器和流分类器有序表的组合，可以实现强大的报文处理功能。为达到更好性能，设计流分类器和流分类器有序表时，应力求尽早匹配原则，使报文匹配尽量在第一个流分类器发生。

关键数据结构
~~~~~~~~~~~~~~~~

在vpp的软件实现里，流分类器表本质上是哈希表，通过类似 ``bihash`` 数据结构实现。用户通过控制面首先创建流分类器，再通过典型的 ``bihash`` 操作，向流分类器表中添加、删除报文流会话；数据面根据流分类器表 ``offset`` ``length`` ``mask`` 定义的报文域，解析报文内容，取得报文关键字，然后在流分类器表中匹配会话规则，获取对应的报文处理动作。

.. figure:: ../images/classify/classify_ds.png
   :align: center

   流分类器数据结构

.. code-block:: c

    typedef struct
    {
      vnet_classify_bucket_t *buckets; // 流分类器 ``bihash`` bucket vec指针
      u32 nbuckets;                    // ``bihash`` bucket数量
      u32 entries_per_page;            // 每个page中会话条目数量
      u32 skip_n_vectors;              // 报文匹配域 ``offset``
      u32 match_n_vectors;             // 报文匹配域 ``length``
      u32 next_table_index;            // 指向流分类器有序列表下一个流分类器
      u32 miss_next_index;             // 匹配未命中时，默认动作
      u32 active_elements;             // 流分类器表中会话条目数量
      union
      {
        u32x4 mask[8];                 // 报文匹配域 ``mask``
        u32 mask_u32[32];
      };
    } vnet_classify_table_t;

``vnet_classify_table_t`` 数据结构实现流分类器表，其中：
	* ``skip_n_vectors`` ``match_n_vectors`` ``mask`` 分别对应于 ``offset`` ``length`` ``mask`` ；  ``skip_n_vectors`` ``match_n_vectors`` 以16字节为单位； ``skip_n_vectors`` 16字节对齐。
	* ``active_elements`` 统计流分类器表中添加的会话条目的数量。
	* ``nbuckets`` 记录控制面传递的 ``bucket`` 数量。设计流分类器时， ``nbuckets`` 一般设置为流分类器中会话条目的容量数目，这样可以避免哈希冲突的可能性，提高报文匹配查找效率。
	* ``buckets`` 指向 ``bucket`` 的内存位置。每个 ``bucket`` 存储2的幂次数量的 ``page`` 。 ``page`` 的数量可以随着会话条目的添加，发生哈希冲突时自动扩大，以解决哈希冲突问题。
	* ``entries_per_page`` 为 ``page`` 中会话条目的数量。vpp的流分类器设置该值固定为2。

.. code-block:: c

	typedef struct
	{
	  union
	  {
		struct
		{
		  u32 offset;         // bucket包含的会话条目数据的地址
		  u8 linear_search;   // 当前bucket进行哈希查找还是线性查找?
		  u8 pad[2];
		  u8 log2_pages;      // bucket中包含的page数量
		};
		u64 as_u64;
	  };
	} vnet_classify_bucket_t;

``vnet_classify_bucket_t`` 为 ``bucket`` 数据结构，其中：
	* ``offset`` 指向 ``page`` 内存位置，每个 ``page`` 存储2个会话条目。
	* ``log2_pages`` 记录 ``bucket`` 包含的 ``page`` 的数量。 ``page`` 为2的幂次，随着会话条目的增加，会自动扩大。
	* ``linear_search`` 标记当前 ``bucket`` 内进行哈希查找，还是线性查找。默认哈希查找，只有当哈希冲突严重时，才会退化成线性查找。

.. code-block:: c

	typedef struct _vnet_classify_entry
	{
	  vnet_classify_action_t action; // 匹配命中时，执行的动作
	  u32 next_index;                // 匹配命中时，转发报文到node-graph的下一node的索引
	  u32x4 key[0];                  // 会话条目关键字
	} vnet_classify_entry_t;

``vnet_classify_entry_t`` 为会话条目数据结构，记录待匹配报文流的键值对，其中：
	* ``key`` 存储报文流的关键字
	* ``action`` 指示一旦匹配命中，对报文采取的动作

代码分析
~~~~~~~~~~~~~~~~

**创建流分类器**

控制面通过CLI或者API调用创建、删除流分类器表，其函数原型如下：

.. code:: c

  int
  vnet_classify_add_del_table (vnet_classify_main_t *cm, const u8 *mask,
  				 u32 nbuckets, u32 memory_size, u32 skip,
  				 u32 match, u32 next_table_index,
  				 u32 miss_next_index, u32 *table_index,
  				 u8 current_data_flag, i16 current_data_offset,
  				 int is_add, int del_chain)

直接调用此函数时，传递流分类器主变量 ``cm = &vnet_classify_main`` 。

参数 ``skip(_n_vectors)`` 、 ``match(_n_vectors)`` 、 ``mask`` 分别对应于定义流分类器报文域的 ``offset`` ``length`` ``mask`` 。 ``skip(_n_vectors)`` 对齐16字节，且以16字节为单位。 ``match(_n_vectors)`` 表示报文域长度，且以16字节为单位。 ``mask`` 标示报文域中有效位，置 ``1`` 有效；必须和match的16字节匹配，长度为16字节整倍数，。

流分类器表基于 ``bihash`` 数据结构。 ``bihash`` 保证数据面报文查找过程中，读操作无锁，且线程安全。

``nbuckets`` 表示哈希存储桶向量的 **固定** 大小。为有效利用内存空间和避免哈希冲突的几率， ``nbuckets`` 的值最好与流分类器设计容纳的会话条目数量相同。目前不支持动态扩充 ``nbuckets``，而是通过动态扩充 ``page`` 来解决哈希冲突。

每个流分类器表都有自己的内存分配区域，流分类器创建在独立申请的内存空间上，可由 ``memory_size`` 指定分配的内存空间大小。

``next_table_index`` 指示下一个需要搜索的流分类器的索引值（内存池 ``vnet_classify_main.tables`` 的索引值）。当前流分类器查找失败时，会进入下一流分类器，继续进行查找。 ``~0`` 表示没有下一流分类器。

特定的流分类器客户端节点将 ``miss_next_inde`` 参数解释为vpp图节点的下一个节点索引。当报文流在流分类器中查找未匹配时， ``ip_classify_inline`` 将报文发送到 ``miss_next_inde`` 指示的节点。

最后， ``is_add`` 参数指示是添加还是删除流分类表。

**创建报文流会话**

控制面通过CLI或者API调用向流分类器表中添加、删除报文流会话条目，其函数原型如下：

.. code:: c

  int
  vnet_classify_add_del_session (vnet_classify_main_t *cm, u32 table_index,
  				   const u8 *match, u32 hit_next_index,
  				   u32 opaque_index, i32 advance, u8 action,
  				   u16 metadata, int is_add)

直接调用此函数时，传递流分类器主变量 ``cm = &vnet_classify_main`` 。

``table_index``表示进行会话条目添加或删除的目标流分类器表； ``is_add`` 参数指示是添加还是删除流分类表会话条目。

``match`` 用于生成会话条目中键值对的关键字，它代表会话条目中，待匹配报文的内容。它不必16字节对齐，但长度必须为流分类器 ``match_n_vectors * 16`` 字节。 ``match`` 与流分类器 ``skip(_n_vectors)`` 、 ``match(_n_vectors)`` 、 ``mask`` 值进行计算取得会话条目键值对的关键字。此关键字经由哈希函数计算得到哈希值，通过哈希值指定键值对存在流分类器的特定位置： ``bucket`` ： ``page`` ： ``entry`` 。

``hit_next_index`` 、 ``opaque_index`` 、 ``advance`` 、 ``action``、 ``metadata`` 都是会话条目键值对的值的组成部分。

特定的流分类器客户端节点将每个会话的 ``hit_next_index`` 参数解释为vpp图节点的下一个节点索引。当数据包分类产生匹配时， ``ip_classify_inline`` 将数据包发送到指定的位置。

流分类器应用程序都可以将会话命中的数据包发送到特定的图节点，并在缓冲区元数据中提供有用的值。比如，ip4_classify或ip6_classify将每个会话的opaque_index参数放入 ``vnet_buffer（b）->l2_classify.opaque_index`` ，根据所需的语义，将已知会话流量发送到某个节点。这完全取决于控制平面和特定的用例。

最后，诸如ip4_classify或ip6_classify之类的节点将 ``advance`` 参数作为有符号参数应用于 ``vlib_buffer_advance(...)`` ， **消费** 报文数据。比如，如果需要针对传入的隧道IP数据包的内部报文进行分类，需要对内部数据包进行解封装和重新封装。在这种情况下，设置 ``advance`` 进行隧道解封装，将报文流转发到 ``next_index`` 指示的节点图的下一个节点。该节点使用 ``opaque_index`` 在特定隧道接口上输出报文流。

**流分类节点处理函数**

流分类器客户端节点报文处理流程基本类似，以ip4_classify和ip6_classify客户端节点为例，其调用函数如下：

.. code:: c

	static inline uword
	ip_classify_inline (vlib_main_t * vm,
				vlib_node_runtime_t * node,
				vlib_frame_t * frame, int is_ip4)

节点处理函数中，首先遍历传入到节点的所有报文，通过流分类器的 ``skip(_n_vectors)`` 、 ``match(_n_vectors)`` 、 ``mask`` 与报文内容进行计算，取得待匹配报文流关键字，然后调用 ``vnet_classify_hash_packet (t0, (u8 *) h0)`` 计算报文关键字的哈希值。

然后，节点处理函数遍历流分类器有序表的各个流分类表，调用 ``e0 = vnet_classify_find_entry (t0, h0, hash0, now)`` 查找流分类器 ``t0`` 中匹配的会话条目 ``e0`` 。

一旦匹配命中，立即退出遍历过程。匹配命中后，将报文转发到 ``e0->next_index`` 指示的节点图的下一结点。若未匹配命中，将报文转发到 ``t0->miss_next_index`` 指示的节点图的下一结点。

代码文件
~~~~~~~~~~~~~~

src/vnet/classify ::

	├── classify.api
	├── classify_api.c
	├── FEATURE.yaml
	├── flow_classify.c
	├── flow_classify.h
	├── flow_classify_node.c
	├── in_out_acl.c
	├── in_out_acl.h
	├── ip_classify.c
	├── pcap_classify.h
	├── policer_classify.c
	├── policer_classify.h
	├── README
	├── trace_classify.h
	├── vnet_classify.c
	└── vnet_classify.h

配置与命令
-------------

网络拓扑
~~~~~~~~~~~~~~~~~~~~

流分类器测试示例采用的网络拓扑如下图所示。

.. figure:: ../images/classify/classify_traffic.png
   :align: center

   流分类器测试网络拓扑


命令介绍
~~~~~~~~~~~~~~~~~~~~

流分类器Classifier主要命令包括：

.. list-table:: 流分类器命令列表
   :widths: 10 40
   :header-rows: 1

   * - 简介
     - 帮助信息
   * - 创建、删除流分类器
     - classify table [miss-next|l2-miss_next|acl-miss-next <next_index>] mask <mask-value> buckets <nn> [skip <n>] [match <n>] [current-data-flag <n>] [current-data-offset <n>] [table <n>] [memory-size <nn>[M][G]] [next-table <n>] [del] [del-chain]
   * - 添加、删除会话条目
     - classify session [hit-next|l2-input-hit-next|l2-output-hit-next|acl-hit-next <next_index>|policer-hit-next <policer_name>] table-index <nn> match [hex] [l2] [l3 ip4] [opaque-index <index>] [action set-ip4-fib-id|set-ip6-fib-id|set-sr-policy-index <n>] [del]
   * - 入接口绑定流分类器
     - set interface input acl intfc <int> [ip4-table <index>] [ip6-table <index>] [l2-table <index>] [ip4-punt-table <index>] [ip6-punt-table <index> [del]
   * - 出接口绑定流分类器
     - set interface output acl intfc <int> [ip4-table <index>] [ip6-table <index>] [l2-table <index>] [del]

* 创建、删除流分类器表

该命令主要参数是mask，它决定流分类器表的 ``offset`` ``length`` ``mask`` 具体值。用户可以使用[hex]自行构建mask值，或使用命令提供的选项[l2]、[l3 ip4]等，由软件计算设置mask值。

::
    ————————————————
    其中mask <mask-value> 的参数如下：
    1.hex 
    2.l2  参数： src dst proto tag1 tag2 ignore-tag1 ignore-tag2 cos1 cos2
    3.l3  参数： ip4 参数： version hdr_length src dst proto tos length fragment_id ttl checksum
      l3  参数： ip6 参数： version traffic-class flow-label src dst proto payload_length hop_limit
    4.l4  参数： src_port
      l4  参数： dst_port
    ————————————————

* 在流分类器表中添加具体会话条目

用于匹配特定报文。

::

  classify session
	- classify session [hit-next|l2-input-hit-next|l2-output-hit-next|acl-hit-next <next_index>|policer-hit-next <policer_name>] table-index <nn> match [hex] [l2] [l3 ip4] [opaque-index <index>] [action set-ip4-fib-id|set-ip6-fib-id|set-sr-policy-index <n>] [del]

* 将流分类器绑定到接口

::

  set interface input acl
    - set interface input acl intfc <int> [ip4-table <index>] [ip6-table <index>] [l2-table <index>] [ip4-punt-table <index>] [ip6-punt-table <index> [del]
  
  set interface output acl
    - set interface output acl intfc <int> [ip4-table <index>] [ip6-table <index>] [l2-table <index>] [del]

.. code-block:: console

    $ git clone https://gerrit.fd.io/r/vpp
    $ cd vpp

* 显示相关流分类器相关信息

::

  show classify tables    show classify tables [index <nn>]
  show inacl              show inacl type [ip4|ip6|l2]
  show outacl             show outacl type [ip4|ip6|l2]

配置实例
~~~~~~~~~~~~~~~~~~~~

::

	classify table acl-miss-next permit mask l3 ip4 src buckets 16
	classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.1
	classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.2
	set int input acl intfc eth0 ip4-table 0

::

	classify table acl-miss-next permit mask l3 ip4 src buckets 16
	classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.1
	classify session acl-hit-next deny table-index 0 match l3 ip4 src 192.81.0.2
	set int output acl intfc eth1 ip4-table 0

参考
----

...


.. [1] https://s3-docs.fd.io/vpp/22.02/usecases/acls.html#
.. [2] https://wiki.fd.io/view/VPP/Introduction_To_N-tuple_Classifiers
