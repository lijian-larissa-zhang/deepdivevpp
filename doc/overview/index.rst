
VPP概述
==================

VPP - Vector Packet Processing，也即向量报文处理，如同网络领域的瑞士军刀，令人惊叹地集合了性能强大、功能完善的网络协议、网络功能和工具集。它提供了极致的高性能数据链路层、网络层和传输层的各种网络功能和弹性扩展性能的能力。VPP同时也提供由网络层协议、传输协议、会话协议和应用层协议组成的主机栈，能够将VPP提供的整个网络和安全功能融入到用户的网络应用程序中。用户可以很容易的通过使用VPP提供的API或开发VPP插件，来使用或扩展VPP提供的功能。

VPP是LFN (Linux Foundation Networking) FD.io (Fast Data Project)项目的核心子项目。FD.io是一个协作型开源项目，主要的目标是为动态、弹性计算环境构建高性能、安全的数据面框架。FD.io项目的几个特色：

	* 降低进入门槛 - 工业级质量标准，跨平台方案，支持多种硬件类型的网络软件栈
	* 加快创新 - 鼓励颠覆性的创造，培养创新精神
	* 针对平台的优化 - 采纳DPDK加速报文输入输出，针对特性CPU架构的调优
	* 广泛的认可度 - 已经被许多企业应用，并提供支持

LFN由原Linux基金会下若干开源网络项目于2018年初合并成立，经过几年发展，已经成为全球最活跃的开源网络社区之一。LFN联合电信运营商、云服务供应商、用户企业、设备供应商、系统集成商共同努力，为网络基础设施和服务的快速更替、部署和落地提供创新平台和构件。除了FD.io，LFN主要的项目还有Anuket 、EMCO、L3AF、ODIM、ONAP、OpenDaylight、Tungsten Fabric、XGVela。

本篇概要介绍VPP，包含其工作原理，快速入门指南，VPP启动配置参数以及如何通过命令行设置VPP和查找对应的命令。

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   principle
   quickstart
   parameters
   command-line
