VPP介绍
============================

FD.io VPP - 矢量报文处理，是一个开源网络项目，提供开箱即用、工业生产级质量的软交换机、路由器、防火墙、负载均衡器等功能。它是LFN FD.io项目的核心子项目。 作为一种高性能的报文处理软件栈，VPP起源于思科内部的一个项目。该项目从2002年开始，历经若干次重构，由思科在2016年推出开源版本。

VPP的特色在于其高性能，经过生产环境验证的成熟技术，模块化设计，丰富的功能集以及灵活的扩展能力。VPP完全运行在用户空间，不受硬件、内核版本限制，可以直接在商用CPU上运行，具有很好的跨平台支持：

	* 支持多种CPU架构，比如Arm64、Power、x86
	* 支持多种Linux发行版本，比如Ubuntu、CentOS
	* 可运行的多种环境中，比如物理机、虚拟机、容器

VPP的高性能网络软件栈，提供覆盖数据链路层、网络层、传输层的丰富网络功能，以及一个主机协议栈实现。它正在广泛而迅速地成为开发网络应用程序的首选软件栈：

* 运行在通用服务器硬件平台之上，在Linux用户空间执行，高性能的纯软件实现的网络软件栈；
* 适宜多种运行环境，比如比如物理机、虚拟机、容器，且一套代码适用所有这些运行环境，无需特殊定制
* 为VM-to-VM虚拟机网络连接提供基于vhost-user & virtio的高性能方案
* 支持所有L2和L3基本功能，和多种报文封装方案
* 利用DPDK进行网络收发加速
* 通过插件机制可扩展功能
* 提供标准的API供控制平面/业务平面进行配置

强大的网络功能集合
------------------------

VPP丰富的网络功能和灵活扩展的框架模型，使得VPP平台可用于构建任何类型的数据包处理应用程序，如路由器、交换机、负载均衡器、防火墙、IDS或主机栈。以下是VPP平台提供的功能的摘要：

* IPv4/IPv6::

    14+ MPPS, single core
    Multimillion entry FIBs
    Input Checks
      Source RPF
      TTL expiration
      header checksum
      L2 length < IP length
      ARP resolution/snooping
      ARP proxy
    Thousands of VRFs
    	Controlled cross-VRF lookups
    Multipath – ECMP and Unequal Cost
    Multiple million Classifiers –
    	Arbitrary N-tuple
    VLAN Support – Single/Double tag

* IPv4::

    GRE, MPLS-GRE, NSH-GRE,
    VXLAN
    IPSEC
    DHCP client/proxy

* IPv6::

    Neighbor discovery
    Router Advertisement
    DHCPv6 Proxy
    L2TPv3
    Segment Routing
    MAP/LW46 – IPv4aas
    iOAM

* MPLS::

    MPLS-o-Ethernet –
      Deep label stacks supported
    
* L2::

    VLAN Support
      Single/ Double tag
      L2 forwarding with EFP/BridgeDomain concepts
    VTR – push/pop/Translate (1:1,1:2, 2:1,2:2)
    Mac Learning – default limit of 50k addresses
    Bridging – Split-horizon group support/EFP Filtering
    Proxy Arp
    Arp termination
    IRB – BVI Support with RouterMac assignment
    Flooding
    Input ACLs
    Interface cross-connect

优异的软件实现和应用
--------------------------

除了上述VPP网络功能总结，VPP网络栈在软件实现、应用领域具有如下特性：

* 优异的容错和服务中软件升级能力

  * 软件即使崩溃也不会导致系统重启
  * 软件更新不需要重新启动系统
  * 开发环境比类似的开发内核代码的环境，更加友好和便于调试
  * 丰富的用户空间调试工具，比如gdb、valgrind、wireshark
  * 可利用广泛使用的内核模块：uio、igb_uio、vfio-pci

* 覆盖数据链路层、网络层、传输层的丰富网络功能

  * 高速路由表、交换表查找
  * 任意报文域的流分类或ACL报文匹配查找功能
  * 控制面、流量管理和Overlay方案

* 支持 `Linux <https://en.wikipedia.org/wiki/Linux>`_ and `FreeBSD <https://en.wikipedia.org/wiki/FreeBSD>`_

  * 支持标准的操作系统接口类型，比如AF_Packet、Tun/Tap & Netmap

* 支持基于 `DPDK <https://www.dpdk.org/>`_ 的网络和加解密硬件

* 支持容器和虚拟化

  * 支持半虚拟化接口以及Vhost、Virtio
  * 支持网卡PCI passthrough功能
  * 支持原生容器接口类型及MemIF

* TCP/UDP主机协议栈

* 同一套源码和安装程序，可适用于各种应用场景

  * 分立的应用类型; 比如 `路由器 <https://en.wikipedia.org/wiki/Router_(computing)>`_ 和 `交换机 <https://en.wikipedia.org/wiki/Network_switch>`_.
  * `云基础架构和网络功能虚拟化 <https://en.wikipedia.org/wiki/Network_function_virtualization>`_
  * `云原生的基础架构 <https://www.cncf.io/>`_
  * 统一的VPP安装程序，即可适用上述所有场景

* 通过 `CSIT <https://wiki.fd.io/view/CSIT#Start_Here>`_ 实现工业生产级的质量保证

  * 全面白盒测试
  * 安装程序段基地址随机化
  * 共享内存段基地址随机化
  * 堆栈边界检查

