VPP快速入门
============================

本章通过若干安装、运行、配置示例，帮助读者快速上手VPP。

环境准备
--------------------

随着CentOS Linux系列发布版本不再更新维护，VPP对CentOS发布版本已经不再支持，比如CI/CD已经删掉了CentOS平台的验证工作，安装包服务器不再提供RPM安装包。
所以建议读者在Ubuntu-20.04或Ubuntu-20.10平台上进行VPP的快速入门尝试。
本章节所有VPP实验都是在Ubuntu-20.04(Focal)环境下进行。如果您已经有安装了Ubuntu-20.04发布版本的机器，并且具有 ``sudo`` 或者 ``root`` 权限，那么您可以直接进入下一节 **安装VPP** 。否则，您可以尝试如下方式安装Ubuntu-20.04虚拟机环境。

读者可以通过 ``virtualbox`` 和 ``vagrant`` 启动安装Ubuntu-20.04发布版的虚拟机::

	$ apt-get install virtualbox vagrant -y
	$ vagrant init ubuntu/focal64

	$ cat Vagrantfile | grep -v "#" | grep -v ^$
	Vagrant.configure("2") do |config|
	  config.vm.box = "ubuntu/focal64"
		config.vm.provider "virtualbox" do |vb|
		  vb.memory = "8192"
		end
	end

	$ vagrant up
	$ vagrant ssh

读者也可以通过 ``virt-install`` 或者 ``virt-manager`` 工具启动安装Ubuntu-20.04的虚拟机。


安装VPP
----------------

VPP可通过两种方式进行安装：

安装包方式安装
~~~~~~~~~~~~~~~~~~~~~

通过安装包安装VPP软件是最方便的一种安装VPP软件方式。VPP的安装包储存在 ``Package Cloud`` 服务器：https://packagecloud.io/fdio。它提供了Ubuntu Linux发布版本的针对Arm64和x86架构的安装包。随着CentOS Linux系列发布版本不再更新维护，VPP v22.02及之后的release和master分支已经不再提供CentOS发布版本的VPP安装包。所以本节介绍的内容都是基于Ubuntu-20.04。

**安装正式发布版**

VPP正式发布版安装包存储在包服务器地址：https://packagecloud.io/fdio/release。首先添加软件包存储服务器仓库地址到包管理工具apt所用的配置文件，再执行 ``apt-get update`` 更新可用软件的数据库::

	curl -s https://packagecloud.io/install/repositories/fdio/release/script.deb.sh | sudo bash
	apt-get update

查看VPP相关的安装包::

	~# apt-cache search vpp
	libvppinfra - Vector Packet Processing--runtime libraries
	libvppinfra-dev - Vector Packet Processing--runtime libraries
	python3-vpp-api - VPP Python3 API bindings
	vpp - Vector Packet Processing--executables
	vpp-api-python - VPP Python API bindings
	vpp-dbg - Vector Packet Processing--debug symbols
	vpp-dev - Vector Packet Processing--development support
	vpp-plugin-core - Vector Packet Processing--runtime core plugins
	vpp-plugin-devtools - Vector Packet Processing--runtime developer tool plugins
	vpp-plugin-dpdk - Vector Packet Processing--runtime dpdk plugin

查看包服务器提供的所有VPP已发布的正式版本::

	~# apt-cache policy vpp libvppinfra vpp-plugin-core vpp-plugin-dpdk
	vpp:
	  Installed: (none)
	  Candidate: 22.02-release
	  Version table:
		 22.02-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 21.10.1-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 21.10-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 21.06-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 21.01.1-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 21.01-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages
		 20.09-release 500
			500 https://packagecloud.io/fdio/release/ubuntu focal/main amd64 Packages

安装v22.02版本的软件包，其中 ``vpp`` 、 ``libvppinfra`` 、 ``vpp-plugin-core`` 、 ``vpp-plugin-dpdk`` 是必须的软件包，其他是可选软件包::

	apt-get install vpp=22.02-release libvppinfra=22.02-release vpp-plugin-core=22.02-release vpp-plugin-dpdk=22.02-release -y

查看、删除已安装软件VPP软件包::

	dpkg -l | grep vpp
	apt-get remove --purge vpp vpp-plugin-core vpp-plugin-dpdk libvppinfra -y

**安装master分支每日构建版本**

``Package Cloud`` 服务器：https://packagecloud.io/fdio/master 已保存VPP master分支每日构造的安装包。安装流程与安装正式发布版本类似::

	curl -s https://packagecloud.io/install/repositories/fdio/master/script.deb.sh | sudo bash
	apt-get update
	apt-get install vpp libvppinfra vpp-plugin-core vpp-plugin-dpdk -y

源码方式安装  
~~~~~~~~~~~~~~~~~~~~~~~

也可以通过下载源码、编译、安装方式安装VPP特定版本和master分支最新代码。下载master分支和v22.02版本源码分别如下::

	git clone https://github.com/FDio/vpp.git
	git clone -b stable/2202 https://github.com/FDio/vpp.git vpp-22.02

安装依赖库，并编译VPP源码::

	cd vpp-22.02
	make install-deps
	make pkg-deb

安装编译生成的安装包::

	$ ls build-root/*.deb -l
	build-root/libvppinfra_22.02.0-9~gc015843c5_amd64.deb
	build-root/libvppinfra-dev_22.02.0-9~gc015843c5_amd64.deb
	build-root/python3-vpp-api_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp-dbg_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp-dev_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp-plugin-core_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp-plugin-devtools_22.02.0-9~gc015843c5_amd64.deb
	build-root/vpp-plugin-dpdk_22.02.0-9~gc015843c5_amd64.deb

	dpkg -i build-root/libvppinfra_22.02.0-9~gc015843c5_amd64.deb \
            build-root/vpp_22.02.0-9~gc015843c5_amd64.deb \
            build-root/vpp-plugin-dpdk_22.02.0-9~gc015843c5_amd64.deb \
            build-root/vpp-plugin-core_22.02.0-9~gc015843c5_amd64.deb

运行
------------

VPP运行在用户态。在生产环境中，单个VPP实例通过DPDK提供的PF或VF PMD驱动访问实际的物理网卡，进行报文收发。在后续的章节中，还将介绍VPP提供的虚拟化网络方案，即通过各种网络接口，提供物理机、虚拟机、容器间的网络连接方案。
本章内容仅通过简单的网络连接方案，向读者介绍VPP的基本工作方式。包括：

  * 通过veth接口连接多个VPP实例和内核协议栈，进行IP路由转发和基于VLAN的二层报文交换
  * 通过物理网卡和DPDK PMD驱动，VPP进行高速网络转发

VPP默认安装完成之后，即以后台服务方式运行::

	~$ sudo service vpp status
	● vpp.service - vector packet processing engine
		 Loaded: loaded (/lib/systemd/system/vpp.service; enabled; vendor preset: enabled)
		 Active: active (running) since Sun 2022-06-19 13:31:11 UTC; 11s ago
		Process: 179047 ExecStartPre=/sbin/modprobe uio_pci_generic (code=exited, status=1/FAILURE)
	   Main PID: 179048 (vpp_main)
		  Tasks: 2 (limit: 9508)
		 Memory: 66.1M
		 CGroup: /system.slice/vpp.service
				 └─179048 /usr/bin/vpp -c /etc/vpp/startup.conf

``systemd`` 服务通过文件 ``/lib/systemd/system/vpp.service`` 控制。VPP应用程序的启动参数配置文件位于 ``/etc/vpp/startup.conf`` 。为方便灵活的控制VPP的启动参数和搭建基于VPP的网络拓拨，我们可以停掉后台服务，通过手动方式启动VPP实例::

	~$ sudo service vpp stop

配置文件startup.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VPP在启动运行时，可以配置许多的参数用于控制VPP运行方式。关于可配置的参数及其解释，请参考专门介绍配置参数章节。这些参数可以直接在VPP运行命令行提供::

	vpp unix {interactive} plugins { plugin dpdk_plugin.so { disable } }

但是因为可提供的参数众多，一般通过专门的配置文件提供启动参数设置::

	vpp -c /etc/vpp/startup.conf

我们可通过安装包提供的默认参数配置文件为基准，根据我们的需要调整参数，再运行VPP实例。 ``/etc/vpp/startup.conf`` 去掉注释行和空行之后，如下::

	~$ cat /etc/vpp/startup.conf | grep -v "#" | grep -v ^$
	unix {
	  nodaemon
	  log /var/log/vpp/vpp.log
	  full-coredump
	  cli-listen /run/vpp/cli.sock
	  gid vpp
	}
	api-trace {
	  on
	}
	api-segment {
	  gid vpp
	}
	socksvr {
	  default
	}
	cpu {
	}

搭建多VPP IP路由转发示例
---------------------------------

.. figure:: ../images/overview/overview_routing.png
   :align: center

.. code-block:: bash
   :linenos:

    简单VPP IP路由功能验证

	------->>>>>>启动VPP
	sudo vpp -c ./startup-vpp1.conf
	sudo vpp -c ./startup-vpp2.conf

	------->>>>>>>>创建veth pair
	sudo ip link add name eth0host type veth peer name eth0vpp
	sudo ip link add name eth1vpp1 type veth peer name eth1vpp2

	-------->>>>>>>>设置veth主机端网络接口
	sudo ip link set dev eth0host down
	sudo ip link set dev eth0host address 02:ed:be:af:01:01
	sudo ip address add 20.20.1.1/24 dev eth0host
	sudo ip link set dev eth0host up

	-------->>>>>>>>配置veth之VPP端网络接口
	sudo vppctl -s /run/vpp/cli-vpp1.sock

	create host-interface name eth0vpp hw-addr 02:ed:be:af:01:02
	set interface state host-eth0vpp up
	set interface ip address host-eth0vpp 20.20.1.2/24

	-------->>>>>>>>配置链接两个VPP的veth之VPP1端网络接口

	create host-interface name eth1vpp1 hw-addr 02:ed:be:af:02:01
	set interface state host-eth1vpp1 up
	set interface ip address host-eth1vpp1 20.20.2.1/24
	quit

	-------->>>>>>>>配置链接两个VPP的veth之VPP2端网络接口
	sudo vppctl -s /run/vpp/cli-vpp2.sock
	create host-interface name eth1vpp2 hw-addr 02:ed:be:af:02:02
	set interface state host-eth1vpp2 up
	set interface ip address host-eth1vpp2 20.20.2.2/24
	quit


	----------->>>>>>>>>>路由条目，包括内核侧和VPP2侧
	ip route add 20.20.2.0/24 via 20.20.1.2 dev eth0host
	sudo vppctl -s /run/vpp/cli-vpp2.sock
	ip route add 20.20.1.0/24 via 20.20.2.1 host-eth1vpp2


	------------>>>>>>>>>> 清空VPP运行环境
	sudo ip link delete eth0host type veth
	sudo ip link delete eth1vpp1 type veth
	ps -ef | grep vpp | awk '{print $2}'| xargs sudo kill


搭建多VPP VLAN交换示例
-----------------------------------

.. figure:: ../images/overview/overview_switching.png
   :align: center

   简单VPP二层交换功能验证

.. code-block:: bash
   :linenos:

	------->>>>>>启动VPP
	sudo vpp -c ./startup-vpp1.conf
	sudo vpp -c ./startup-vpp2.conf

	------->>>>>>>>创建veth pair
	sudo ip link add name eth0host type veth peer name eth0vpp
	sudo ip link add name eth1vpp1 type veth peer name eth1vpp2

	-------->>>>>>>>设置veth主机端网络接口
	sudo ip link set dev eth0host down
	sudo ip link set dev eth0host address 02:ed:be:af:01:01
	sudo ip address add 20.20.1.1/24 dev eth0host
	sudo ip link set dev eth0host up

	-------->>>>>>>>配置veth之VPP端网络接口
	sudo vppctl -s /run/vpp/cli-vpp1.sock

	create host-interface name eth0vpp hw-addr 02:ed:be:af:01:02
	set interface state host-eth0vpp up
	set interface l2 bridge host-eth0vpp 32

	-------->>>>>>>>配置链接两个VPP的veth之VPP1端网络接口

	create host-interface name eth1vpp1 hw-addr 02:ed:be:af:02:01
	set interface state host-eth1vpp1 up
	set interface l2 bridge host-eth1vpp1 32
	quit

	-------->>>>>>>>配置链接两个VPP的veth之VPP2端网络接口
	sudo vppctl -s /run/vpp/cli-vpp2.sock
	create host-interface name eth1vpp2 hw-addr 02:ed:be:af:02:02
	set interface state host-eth1vpp2 up
	set interface l2 bridge host-eth1vpp2 32

	create loopback interface
	set interface state loop0 up
	set interface ip address loop0 20.20.1.2/24
	set interface l2 bridge loop0 32 bvi

	quit


	------------>>>>>>>>>> 清空VPP运行环境
	sudo ip link delete eth0host type veth
	sudo ip link delete eth1vpp1 type veth
	ps -ef | grep vpp | awk '{print $2}'| xargs sudo kill



单VPP软件收发报文测试示例
-------------------------------------

.. figure:: ../images/overview/overview_packet.png
   :align: center

   通过软件发包工具测试VPP功能


基于物理网卡收发报文
-----------------------------

以intel XL710为例，介绍VPP通过网卡收发报文的配置和命令。作者的物理服务器，安装了一块intel 40G XL710网卡，每个端口分别连接IXIA报文测试仪。

.. figure:: ../images/overview/overview_traffic.png
   :align: center

   基于物理网卡和测试仪的拓扑结构

这块网卡的信息如下::

	$ sudo lshw -c network -businfo
		Bus info          Device           Class          Description
		=============================================================
		pci@0000:b1:00.0  ens5f0           network        Ethernet Controller XL710 for 40GbE QSFP+
		pci@0000:b1:00.1  ens5f1           network        Ethernet Controller XL710 for 40GbE QSFP+

在启动VPP使用该物理网卡之前，需要保证网卡处于link-down状态::

	$ sudo ip link set ens5f0 down
	$ sudo ip link set ens5f1 down

添加网卡信息到启动配置文件，并重命名网口分别为 ``eth0`` 、 ``eth1`` 。规划 ``main`` 线程运行在 ``CPU 0`` ，worker线程运行在 ``CPU 1`` ::

	$ cat startup-phys.conf
		unix {
		  nodaemon
		  log /var/log/vpp/vpp.log
		  full-coredump
		  cli-listen /run/vpp/cli.sock
		  gid vpp
		}
		api-trace {
		  on
		}
		api-segment {
		  gid vpp
		}
		socksvr {
		  default
		}
		cpu {
		  main-core 0
		  corelist-workers 1
		}
		dpdk {
		  dev 0000:b1:00.0 {
			name eth0
		  }
		  dev 0000:b1:00.1 {
			name eth1
		  }
		}

运行VPP之后，可见VPP已经接管该网卡 ::

	$ vpp -c ./startup-phys.conf
	vpp# show interface
				  Name               Idx    State  MTU (L3/IP4/IP6/MPLS)     Counter          Count
	eth0                              1     down         8996/0/0/0
	eth1                              2     down         8996/0/0/0

在IXIA发包仪上，模拟两台主机分别与VPP的网络口 eth0 eth1 直连::

	* 主机1的IP地址 ``192.81.0.1`` ，MAC地址 ``00:00:0A:81:0:1`` ，与eth0直连
	* 主机1的IP地址 ``192.82.0.1`` ，MAC地址 ``00:00:0A:82:0:1`` ，与eth0直连

通过 ``vppctl`` 进入VPP配置命令行，配置网络口IP地址和link状态::

	$ vppctl -s /run/vpp/cli.sock
	vpp# set int ip address eth0 192.81.255.1/16
	vpp# set int ip address eth1 192.82.255.1/16
	vpp# set int state eth0 up
	vpp# set int state eth1 up
	vpp# set interface promiscuous on eth0
	vpp# set interface promiscuous on eth1
	vpp# set ip neighbor eth0 192.81.0.1 00:00:0A:81:0:1 static count 1
	vpp# set ip neighbor eth1 192.82.0.1 00:00:0A:82:0:1 static count 1

完成上述配置之后，即可通过发包测试仪发送流量，测试验证VPP的转发性能。