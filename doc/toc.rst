目录
====

1. VPP概述
	A. VPP介绍
	#. VPP工作原理
	#. VPP入门实践
	#. VPP配置参数
	#. VPP命令行介绍
#. VPP软件架构
	A. 基础数据结构
		a. 基本数据类型
		#. 基本数据结构： vec
		#. 基本数据结构： pool
		#. 基本数据结构： bitmap
		#. 基本数据结构： hash
		#. 基本数据结构： bihash
		#. 基本数据结构： heap
		#. 基本数据结构： fifo
		#. 基本数据结构： string
		#. 基本数据结构： timer
		#. 基本数据结构： vector
		#. 基于dlmalloc的内存分配器
		#. DMA内存分配器： pmalloc
		#. 格式化输入输出： format/unformat
	#. 基础软件功能
		a. 报文缓存管理
		#. 节点管理功能
		#. Feature和FeatureArc机制
		#. 物理内存管理
		#. 报文跟踪调试功能
		#. 计数功能
		#. 日志功能
		#. 命令行功能
	#. 基本网络功能
		a. 网络节点管理
		#. 网络节点示例： IPv4路由
		#. FIB
		#. Segment routing
		#. Punting Packets
	#. 扩展网络插件
		a. 插件管理
		#. 插件开发介绍
		#. 网络插件示例： ACL
	#. 网络接口类型
		a. DPDK
		#. memif - 严晓峰
		#. af-packet
		#. AVF
		#. RDMA
		#. AF-XDP
		#. ...
	#. 编译系统
	#. 测试用例
#. 应用编程接口
	A. C API
	#. C++ API
	#. Python API
	#. go API
#. VPP网络功能
	A. VLAN功能
	#. L2转发
	#. IPv4
	#. IPv6
	#. MAC地址学习
	#. L2泛洪
	#. EFP/Bridge-Domain
	#. Interface Cross-Connect
	#. Proxy ARP/ARP Termination
	#. IPSec - 业平
	#. Classify
	#. VxLAN
	#. NAT
	#. DHCP
	#. GRE
	#. ... 太多了
#. VPP主机协议栈
#. 应用案例
	A. 容器网络方案
	#. 虚拟机网络方案
	#. 家庭网关应用
	#. 加速Web服务器方案
	#. 云实例应用
	#. 安全路由器方案
	#. ... 太多了
#. 其他FD.io项目简介